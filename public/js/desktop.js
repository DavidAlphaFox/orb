/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var _orb_desktop_path;
var orb_read_only;

/* Icon
 */
function orb_icon_coord_to_grid(coord, grid_size) {
	var delta = coord % grid_size;
	coord -= delta;

	if (delta > (grid_size >> 1)) {
		coord += grid_size;
	}

	return coord;
}

/* Refresh desktop
 */
function orb_desktop_refresh() {
	orb_directory_list(_orb_desktop_path, function(items) {
		var desktop = $('div.desktop div.icons');

		desktop.empty();

		/* Fill explorer
		 */
		items.forEach(function(item) {
			if (item.type == 'directory') {
				var icon = orb_file_make_icon(item, _orb_desktop_path, 'directory');
				desktop.append(icon);
			}
		});

		items.forEach(function(item) {
			if (item.type == 'file') {
				var icon = orb_file_make_icon(item, _orb_desktop_path, 'file');
				desktop.append(icon);
			}
		});

		var y = 0;
		$('div.desktop div.icons div.icon').each(function() {
			var width = Math.round($(this).outerWidth());
			var height = Math.round($(this).innerHeight() - $(this).find('span').innerHeight()) + 30;

			$(this).css('top', (y++ * height) + 'px');

			/* Drag file
			 */
			if (orb_read_only == false) {
				$(this).draggable({
					containment: 'parent',
					helper: 'clone',
					handle: 'img',
					zIndex: 10000,
					start: function() {
						orb_startmenu_close();
					},
					stop: function(event, ui) {
						if (orb_key_pressed(KEY_CTRL)) {
							return;
						}

						var pos = $(ui.helper).position();

						pos.top = orb_icon_coord_to_grid(pos.top, height);
						pos.left = orb_icon_coord_to_grid(pos.left, width);

						var win_y = $('div.icons').height();
						var icon = $(this);

						$('div.icons div.icon').each(function() {
							if ($(this).is(icon)) {
								return true;
							}

							var other = $(this).position();
							if ((pos.top == other.top) && (pos.left == other.left)) {
								pos.top += height;
							}
							if (pos.top + height > win_y) {
								pos.top -= height;
								pos.left += width;
							}
						});

						$(this).css('top', pos.top + 'px');
						$(this).css('left', pos.left + 'px');
					}
				});
			}
		});

		/* Click file on mobile device
		 */
		if ($('div.desktop').attr('mobile') == 'yes') {
			$('div.desktop div.icons div.icon').click(function() {
				var filename = _orb_desktop_path + '/' + $(this).find('span').text();

				if (desktop.data('click_last') == filename) {
					desktop.data('click_last', null);
					$(this).trigger('dblclick');
				} else {
					desktop.data('click_last', filename);
				}
			});
		}

		/* Double click file
		 */
		$('div.desktop div.icons div.icon').dblclick(function() {
			var filename = _orb_desktop_path + '/' + $(this).find('span').text();
			var type = $(this).attr('type');

			if (type == 'file') {
				var extension = orb_file_extension(filename);

				if ((handler = orb_get_file_handler(extension)) != undefined) {
					handler(filename);
				} else {
					window.open('/orb/file/download/' + url_encode(filename), '_blank').focus();
				}
			} else {
				if ((handler = orb_get_directory_handler()) != undefined) {
					handler(filename);
				}
			}
		});

		/* Right click
		 */
		$('div.desktop div.icons div.icon').contextmenu(function(event) {
			orb_startmenu_close();

			var menu_entries = [];
			if ($(this).attr('type') == 'file') {
				menu_entries.push({ name: 'Download', icon: 'download' });
			}
			menu_entries.push({ name: 'Rename', icon: 'edit' });
			menu_entries.push({ name: 'Delete', icon: 'remove' });

			orb_contextmenu_add_items(menu_entries, $(this).attr('ext'));
			orb_contextmenu_show($(this), event, menu_entries, orb_desktop_contextmenu_handler);
			return false;
		});
	}, function(result) {
		orb_alert('The directory "' + _orb_desktop_path + '" is missing in your home directory.', 'Error');
	});
}

/* Rearrange windows and icons on the desktop
 */
function orb_desktop_rearrange() {
	/* Rearrange windows
	 */
	var windows_width = Math.round($('div.windows').width());
	var windows_height = Math.round($('div.windows').height());

	$('div.windows div.window').each(function() {
	    if ((windat = $(this).data('maximize')) != undefined) {
			$(this).removeData('maximize');
			orb_window_maximize($(this).attr('id'));
			$(this).data('maximize', windat);
			return true;
		}

		var pos = $(this).position();
		var width = Math.round($(this).outerWidth());
		var height = Math.round($(this).outerHeight());

		if (pos.left + width > windows_width) {
			pos.left = windows_width - width;
		}

		if (pos.left < 0) {
			pos.left = 0;
			if ($(this).is('.ui-resizable')) {
				if (pos.left + width > windows_width) {
					$(this).css('width', windows_width + 'px');
				}
			}
		}

		if (pos.top + height > windows_height) {
			pos.top = windows_height - height;
		}

		if (pos.top < 0) {
			pos.top = 0;
			if ($(this).is('.ui-resizable')) {
				if (pos.top + height > windows_height) {
					$(this).css('height', windows_height + 'px');
				}
			}
		}

		$(this).css('top', pos.top + 'px');
		$(this).css('left', pos.left + 'px');
	});

	/* Rearrange icons
	 */
	var icons_width = Math.round($('div.icons').width());
	var icons_height = Math.round($('div.icons').height());

	$('div.icons div.icon').each(function() {
		var pos = $(this).position();
		var width = Math.round($(this).outerWidth());
		var height = Math.round($(this).outerHeight());

		while (pos.left + width >= icons_width) {
			pos.left -= width;
			if (pos.left < 0) {
				pos.left = 0;
				break;
			}
		}
		
		while (pos.top + height >= icons_height) {
			pos.top -= height;
			if (pos.top < 0) {
				pos.top = 0;
				break;
			}
		}

		$(this).css('top', pos.top + 'px');
		$(this).css('left', pos.left + 'px');
	});
}

/* Load wallpaper
 */
function orb_desktop_load_wallpaper(wallpaper) {
	if (wallpaper.substring(0, 1) == '/') {
		wallpaper = wallpaper.substring(1);
	}

	$('div.desktop').css('background-image', 'url(/orb/file/download/' + url_encode(wallpaper) + ')');
}

/* Menu handler
 */
function orb_desktop_contextmenu_handler(icon, option) {
	var filename = icon.find('span').text();

	switch (option) {
		case 'Download':
			var url = '/orb/file/download/' + _orb_desktop_path + '/' + url_encode(filename);
			window.open(url, '_blank').focus();
			break;
		case 'Rename':
			var new_filename = prompt('Rename file', filename);
			if (new_filename !== null) {
				orb_file_rename(_orb_desktop_path + '/' + filename, new_filename, undefined, function() {
					orb_alert('Error while renaming file or directory.', 'Error');
				});
			}
			break;
		case 'Delete':
			if (confirm('Delete ' + filename + '?')) {
				if (icon.attr('type') == 'file') {
					orb_file_remove(_orb_desktop_path + '/' + filename, undefined, function() {
						orb_alert('Error while deleting file.', 'Error');
					});
				} else {
					orb_directory_remove(_orb_desktop_path + '/' + filename, undefined, function() {
						orb_alert('Error while deleting directory.', 'Error');
					});
				}
			}
			break;
	}
}

/* Main
 */
$(document).ready(function() {
	_orb_desktop_path = $('div.desktop').attr('path');
	orb_read_only = $('div.desktop').attr('read_only') == 'yes';

	orb_setting_get('system/wallpaper', function(wallpaper) {
		orb_desktop_load_wallpaper(wallpaper);
	}, function() {
		orb_alert('Error loading wallpaper.', 'Error');
	});

	orb_setting_get('system/color', function(color) {
		orb_window_set_color(color);
	}, function() {
		orb_alert('Error loading window color.', 'Error');
		orb_window_set_color('#808080');
	});

	orb_desktop_refresh();

	/* Droppable
	 */
	if (orb_read_only == false) {
		$('div.desktop').droppable({
			accept: 'div.icon, div.detail',
			drop: function(event, ui) {
				var span = ui.helper.find('span').first();
				var source_filename = span.text();
				var source_path = span.attr('path');
				var source = source_path + '/' + source_filename;

				if (source_path == 'Desktop') {
					return;
				}

				orb_file_exists('Desktop/' + source_filename, function(exists) {
					if (exists) {
						if (confirm('Destination file already exists. Overwrite?') == false) {
							return;
						}
					}

					if (orb_key_pressed(KEY_CTRL)) {
						orb_file_copy(source, _orb_desktop_path, undefined, function() {
							orb_alert('Error copying file.', 'Error');
						});
					} else {
						orb_file_move(source, _orb_desktop_path, undefined, function() {
							orb_alert('Error moving file.', 'Error');
						});
					}
				});
			}
		});
	}

	/* Left click
	 */
	$('div.desktop').on('click', function() {
		orb_startmenu_close();
	});

	$('div.desktop').on('contextmenu', function() {
		orb_startmenu_close();
	});

	/* Resize browser window
	 */
	var resize = 0;
	$(window).on('resize', function() {
		var current = ++resize;
		setTimeout(function() {
			if (current != resize) {
				return;
			}

			orb_desktop_rearrange();
		}, 100);
	});

	/* File changes
	 */
	orb_directory_upon_update(function(directory) {
		if (directory == _orb_desktop_path) {
			orb_desktop_refresh();
		}
	});

	/* Drop files
	 */
	if (orb_read_only == false) {
		$('div.desktop').on('dragover', function(event) {
			var explorers = 0;
			$('div.explorer').each(function() {
				explorers++;
			});

			if (explorers == 0) {
				explorer_open(_orb_desktop_path);
			}

			event.preventDefault();
			event.stopPropagation();
		});

		$('div.desktop').on('drop', function(event) {
			event.preventDefault();
			event.stopPropagation();
		});
	}
});
