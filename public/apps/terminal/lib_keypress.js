/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var _terminal_input_callback = undefined;
var _terminal_input_echo = undefined;
var _terminal_input = '';

function terminal_write_command(term) {
	var cols = term.getOption('cols');

	var line = '\r' + TERMINAL_PROMPT;
	line += term.command.padEnd(cols - TERMINAL_PROMPT_LENGTH, ' ');
	line += ''.padEnd(cols - TERMINAL_PROMPT_LENGTH - term.command.length - 1, '\b');

	term.write(line);
}

function terminal_input(callback, echo = true) {
	_terminal_input_callback = callback;
	_terminal_input_echo = echo;
}

function terminal_input_keypress(term, keycode) {
	if (keycode == 8) {
		/* Backspace
		 */
		var len = _terminal_input.length;
		if (_terminal_input_echo && (len > 0)) {
			term.write('\b \b');
		}
		_terminal_input = _terminal_input.substring(0, len - 1);
	} else if (keycode == 13) {
		/* Enter
		 */
		term.writeln('');
		_terminal_input_callback(_terminal_input);

		_terminal_input_callback = undefined;
		_terminal_input_echo = undefined;
		_terminal_input = '';
	} else if (keycode == 27) {
		/* Escape
		 */
		_terminal_input_callback(undefined);

		_terminal_input_callback = undefined;
		_terminal_input_echo = undefined;
		_terminal_input = '';
	} else {
		/* Other key
		 */
		_terminal_input += event.key;
		if (_terminal_input_echo) {
			term.write(event.key);
		}
	}
}

function terminal_keypress(terminal_window, term, event) {
	if (term.busy) {
		if (_terminal_input_callback != '') {
			terminal_input_keypress(term, event.domEvent.keyCode);
		}
		return;
	}

	if (event.domEvent.keyCode == 8) {
		/* Backspace
		 */
		var len = term.command.length;
		if (len == 0) {
			return;
		}

		var cols = term.getOption('cols');
		if ((len + TERMINAL_PROMPT_LENGTH) % cols == 0) {
			term.write('\x9B1A\x9B' + (cols - 1) + 'C ');
		} else {
			term.write('\b \b');
		}

		term.command = term.command.substring(0, len - 1);
	} else if (event.domEvent.keyCode == 9) {
		/* Tab
		 */
		terminal_complete_input(term);
	} else if (event.domEvent.keyCode == 13) {
		/* Enter
		 */
		if (term.command.trim() != '') {
			var index = term.history.indexOf(term.command);
			if (index != -1) {
				term.history.splice(index, 1);
			}

			term.history_pointer = term.history.length - 1;
			term.history[term.history_pointer++] = term.command;
			term.history[term.history_pointer] = '';
		}

		term.writeln('');

		terminal_command(terminal_window, term);
	} else if (event.domEvent.keyCode == 33) {
		/* PageUp
		 */
	} else if (event.domEvent.keyCode == 34) {
		/* PageDown
		 */
		term.scrollPages(1);
	} else if (event.domEvent.keyCode == 33) {
		/* PageUp
		 */
		term.scrollPages(-1);
	} else if (event.domEvent.keyCode == 35) {
		/* End
		 */
		term.scrollToBottom();
	} else if (event.domEvent.keyCode == 36) {
		/* Home
		 */
		term.scrollToTop();
	} else if (event.domEvent.keyCode == 37) {
		/* Left
		 */
	} else if (event.domEvent.keyCode == 38) {
		/* Up
		 */
		if (term.history_pointer == 0) {
			return;
		}

		var max_ptr = term.history.length - 1;
		if (term.history_pointer == max_ptr) {
			term.history[max_ptr] = term.command;
		}

		term.command = term.history[--term.history_pointer];

		terminal_write_command(term);
	} else if (event.domEvent.keyCode == 39) {
		/* Right
		 */
	} else if (event.domEvent.keyCode == 40) {
		/* Down
		 */
		var max_ptr = term.history.length - 1;
		if (term.history_pointer == max_ptr) {
			return;
		}

		term.command = term.history[++term.history_pointer];

		terminal_write_command(term);
	} else if (event.domEvent.keyCode == 45) {
		/* Insert
		 */
	} else if (event.domEvent.keyCode == 46) {
		/* Delete
		 */
	} else if (event.domEvent.keyCode == 116) {
		/* F5
		 */
		location.reload();
	} else {
		/* Other key
		 */
		term.command += event.key;
		term.write(event.key);
	}
}
