/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var audio_extensions = [ 'mpga', 'mpega', 'mp2', 'mp3', 'm4a', 'wma', 'wav' ];

function audio_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return audio_extensions.includes(extension.toLowerCase());
}

function audio_play(audio_window, filename) {
	var parts = filename.split('/');
	var name = parts.pop();

	if (filename.substr(0, 1) == '/') {
		filename = filename.substr(1);
	}

	audio_window.find('div.name').text(name);
	audio_window.find('audio').remove();
	audio_window.append('<audio controls><source src="' + orb_download_url(filename) + '" /></audio>');
	audio_window.find('audio')[0].play();
}

function audio_menu_click(audio_window, item) {
	audio_window = audio_window.find('div.audio');

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (audio_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					audio_play(audio_window, filename);
				}
			});
			break;
		case 'Exit':
			audio_window.close();
			break;
		case 'About':
			orb_alert('Audio player\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function audio_open(filename = undefined) {
	var window_content =
		'<div class="audio">' +
		'<div class="name"></div>' +
		'<audio controls></audio>' +
		'</div>';

	var audio_window = $(window_content).orb_window({
		header:'Audio',
		icon: '/apps/audio/audio.png',
		width: 400,
		height: 100,
		maximize: false,
		resize: false,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: audio_menu_click
	});

	audio_window.open();

	if ($('div.desktop').attr('mobile') == 'yes') {
		var windowframe = audio_window.parent().parent();
		windowframe.css('height', (windowframe.height() + 15) + 'px');
	}

	if (filename != undefined) {
		audio_play(audio_window, filename);
	}
}

$(document).ready(function() {
	var icon = '/apps/audio/audio.png';
	orb_startmenu_add('Audio', icon, audio_open);
	audio_extensions.forEach(function(extension) {
		orb_upon_file_open(extension, audio_open, icon);
	});
});
