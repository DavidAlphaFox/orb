/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function minesweeper_new_minefield(width, height, mines, no_x, no_y) {
	var line = new Array();
	for (var y = 0; y < height; y++) {
		line.push(false);
	}

	var minefield = new Array();
	for (var x = 0; x < width; x++) {
		minefield[x] = new Array();
		minefield[x] = line.map((i) => i);
	}

	while (mines > 0) {
		var x = Math.floor(Math.random() * width);
		var y = Math.floor(Math.random() * height);

		if ((x == no_x) && (y == no_y)) {
			continue;
		}

		if (minefield[x][y] == false) {
			minefield[x][y] = true;
			mines--;
		}
	}

	return minefield;
}

function minesweeper_cell_value(minefield, x, y) {
	var value = 0, i, j;

	for (var i = x - 1; i <= x + 1; i++) {
		if ((i < 0) || (i >= minefield.length)) {
			continue;
		}

		for (var j = y - 1; j <= y + 1; j++) {
			if ((j < 0) || (j >= minefield[i].length)) {
				continue;
			}

			if (minefield[i][j] === true) {
				value++;
			}
		}
	}

	return value;
}

function minesweeper_flood(minesweeper_window, minefield, x, y) {
	for (var i = x - 1; i <= x + 1; i++) {
		for (var j = y - 1; j <= y + 1; j++) {
			minesweeper_window.find('button[x=' + i +'][y=' + j + ']').each(function() {
				$(this).click();
			});
		}
	}
}

function minesweeper_done(minesweeper_window) {
	minesweeper_window.find('button').off('click');
	minesweeper_window.find('button').off('contextmenu');
}

function minesweeper_check_victory(minesweeper_window, mines) {
	var buttons = minesweeper_window.find('button').length;
	var flags = minesweeper_window.find('button.flag').length;

	minesweeper_window.find('input.flags').val(flags);

	if ((buttons == flags) && (flags == mines)) {
		minesweeper_done(minesweeper_window);
		minesweeper_window.find('table').addClass('victory');
	}
}

function minesweeper_init(minesweeper_window, width, height, mines) {
	minesweeper_window.empty();

	var minefield = '<div class="counters">' +
		'<span>Mines: <input type="text" class="mines" disabled="disabled" /></span>' +
		'<span>Flags: <input type="text" class="flags" disabled="disabled" /></span>' +
		'</div><table class="minefield">\n';
	for (var y = 0; y < height; y++) {
		minefield += '<tr>';
		for (var x = 0; x < width; x++) {
			minefield += '<td><button x="' + x + '" y="' + y + '" mark="none"></button></td>';
		}
		minefield += '</tr>\n';
	}
	minefield += '</table>\n';

	minesweeper_window.append(minefield);

	windowframe = minesweeper_window.parent().parent();
	windowframe.css('width', (width * 25 + 20) + 'px');
	windowframe.css('height', (height * 25 + 110) + 'px');

	minefield = undefined;

	minesweeper_window.find('input.mines').val(mines);
	minesweeper_window.find('input.flags').val(0);

	/* Left click
	 */
	minesweeper_window.find('table.minefield button').click(function() {
		if ($('div.desktop').attr('mobile') == 'yes') {
			var place_flag = minesweeper_window.find('div.switcher input').prop('checked');
			if (place_flag) {
				$(this).trigger('contextmenu');
				return;
			}
		}

		var x = parseInt($(this).attr('x'));
		var y = parseInt($(this).attr('y'));

		if (minefield == undefined) {
			minefield = minesweeper_new_minefield(width, height, mines, x, y);
		}

		var mark = $(this).attr('mark');
		if (mark != 'none') {
			return;
		}

		if (minefield[x][y]) {
			$(this).addClass('mine');
			minesweeper_window.find('table').addClass('boom');
			minesweeper_done(minesweeper_window);
			return;
		}

		var value = minesweeper_cell_value(minefield, x, y)

		var cell = $(this).parent();
		$(this).remove();

		if (value == 0) {
			minesweeper_flood(minesweeper_window, minefield, x, y);
		} else {
			cell.append(value);
		}

		minesweeper_check_victory(minesweeper_window, mines);
	});

	/* Right click
	 */
	minesweeper_window.find('table.minefield button').contextmenu(function(event) {
		var mark = $(this).attr('mark');

		switch (mark) {
			case 'none':
				$(this).addClass('flag');
				$(this).attr('mark', 'flag');
				break;
			case 'flag':
				$(this).removeClass('flag');
				$(this).attr('mark', 'none');
				break;
		}

		minesweeper_check_victory(minesweeper_window, mines);

		return false;
	});

	/* Mobile
	 */
	if ($('div.desktop').attr('mobile') == 'yes') {
		windowframe.css('height', (windowframe.height() + 35) + 'px');

		minesweeper_window.append('<div class="switcher"><input type="checkbox" /> Place flag</div>');
	}
}

function minesweeper_open(filename = undefined) {
	var window_content =
		'<div class="minesweeper"></div>';

	var minesweeper_window = $(window_content).orb_window({
		header:'Minesweeper',
		icon: '/apps/minesweeper/minesweeper.png',
		width: 245,
		height: 255,
		resize: false,
		maximize: false,
		menu: {
			'File': [ 'Beginner', 'Intermediate', 'Expert', '-', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: minesweeper_menu_click
	});

	minesweeper_init(minesweeper_window, 9, 9, 10);
	minesweeper_window.open();

	minesweeper_window.contextmenu(function(event) {
		return false;
	});
}

function minesweeper_menu_click(minesweeper_window, item) {
	minesweeper_window = minesweeper_window.find('div.minesweeper');

	switch (item) {
		case 'Beginner':
			minesweeper_init(minesweeper_window, 9, 9, 10);
			break;
		case 'Intermediate':
			minesweeper_init(minesweeper_window, 16, 16, 40);
			break;
		case 'Expert':
			minesweeper_init(minesweeper_window, 30, 16, 99);
			break;
		case 'Exit':
			minesweeper_window.close();
			break;
		case 'About':
			orb_alert('Minesweeper\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

$(document).ready(function() {
	orb_startmenu_add('Minesweeper', '/apps/minesweeper/minesweeper.png', minesweeper_open);
});
