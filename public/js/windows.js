/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var _orb_window_id_label = 'windowframe';

/* About
 */
function orb_window_about(win) {
	var title = win.find('div.window-header div.title').text();
	var id = win.find('div.window-body > div').data('windowframe_id');

	orb_alert('Application: ' + title + '\nProcess ID: ' + id, 'Window information');
}

/* Get window max z-index
 */
function orb_window_max_zindex() {
	var max_zindex = 0;

	$('div.windows > div').each(function() {
		var zindex = parseInt($(this).css('z-index'));
		if (isNaN(zindex) == false) {
			if (zindex > max_zindex) {
				max_zindex = zindex;
			}
		}
	});

	return max_zindex;
}

/* Raise window
 */
function orb_window_raise(windowframe) {
	if (windowframe.hasClass('focus')) {
		return;
	}

	if ($('div.windows > div.dialog').length > 0) {
		if (windowframe.hasClass('dialog') == false) {
			return;
		}
	}

	var zindex = orb_window_max_zindex() + 1;
	windowframe.css('z-index', zindex);

	$('div.windows div.window').removeClass('focus');
	windowframe.addClass('focus');

	orb_taskbar_focus(windowframe.prop('id'));
}

/* Maximize window
 */
function orb_window_maximize(window_id) {
	var windowframe = $('div.windows div#' + window_id);

	if ((windat = windowframe.data('maximize')) == undefined) {
		var pos_x = 0;
		var pos_y = 0;
		var width = Math.round($('div.windows').width());
		var height = Math.round($('div.windows').height());

		var pos = windowframe.position();
		var windat = [ pos.left, pos.top, windowframe.width(), windowframe.height() ];
		windowframe.data('maximize', windat);
	} else {
		var pos_x = windat[0];
		var pos_y = windat[1];
		var width = windat[2];
		var height = windat[3];
		windowframe.removeData('maximize');
	}

	windowframe.css('left', pos_x + 'px');
	windowframe.css('top', pos_y + 'px');
	windowframe.css('width', width + 'px');
	windowframe.css('height', height + 'px');
}

/* Minimize window
 */
function orb_window_minimize(window_id) {
	$('div.taskbar div.tasks div.task[taskid=' + window_id + ']').addClass('minimized');

	var task = $('div.windows div#' + window_id);
	task.hide();
}

/* Set window color
 */
function orb_window_set_color(color) {
	$('head style#orb_window_color').remove();

	var style = '<style id="orb_window_color" type="text/css">\n' +
		'div.windows div.window div.window-header { background-color: ' + color + ' }\n' +
		'</style>';

	$('head').append(style);
}

/* Window plugin
 */
(function($) {
	const MARGIN_BOTTOM = 30;

	var pluginName = 'orb_window';
	var defaults = {
		activator: undefined,
		top: undefined,
		width: 600,
		minwidth: 400,
		height: undefined,
		bgcolor: undefined,
		style: 'primary',
		header: 'Application',
		icon: undefined,
		menu: undefined,
		open: undefined,
		close: undefined,
		maximize: undefined,
		minimize: undefined,
		resize: undefined,
		dialog: false
	};

	var mouse_offset_x;
	var mouse_offset_y

	/* Constructor
	 */
	var plugin = function(el, options) {
		var element = $(el);
		var settings = $.extend({}, defaults, options);
		var id = 1;
		while ($('div.windows div#' + _orb_window_id_label + id).length > 0) {
			id++;
		}

		if (settings.dialog == true) {
			settings.minimize = false;
		}

		element.data('windowframe_id', id);

		var menu = '';
		if (settings.menu != undefined) {
			menu += '<ul class="nav nav-tabs">';
			for ([item, entries] of Object.entries(settings.menu)) {
				menu += '<li role="presentation" class="dropdown">' +
				        '<a class="dropdown-toggle" data-toggle="dropdown" href="#" onClick="javascript:return false" role="button" ' +
				        'aria-haspopup="true" aria-expanded="false" ondragstart="return false">' +
				        item + '<span class="caret"></span>' +
				        '</a><ul class="dropdown-menu">';
				entries.forEach(function(entry) {
					if (entry == '-') {
						menu += '<li role="separator" class="divider"></li>';
					} else {
						menu += '<li><a class="entry" href="#" onClick="javascript:return false" ondragstart="return false">' + entry + '</a></li>';
					}
				});
				menu += '</ul></li>';
			};
			menu += '</ul>';
		}

		/* Dialog
		 */
		if (settings.dialog) {
			var zindex = orb_window_max_zindex() + 1;
			var overlay = '<div class="overlay overlay' + id + '" style="z-index:' + zindex + '"></div>';
			$('div.windows').append(overlay);
			$('div.taskbar').append(overlay);
			$('div.overlay').click(function(event) {
				event.stopPropagation();
			});
			$('div.overlay').contextmenu(function(event) {
				event.stopPropagation();
				return false;
			});
		}

		/* Window frame
		 */
		var window_buttons =
			(settings.close === false ? '' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>') +
			(settings.maximize === false ? '' : '<span class="fa fa-window-maximize"></span>') +
			(settings.minimize === false ? '' : '<span class="fa fa-window-minimize"></span>');
		var icon = (settings.icon == undefined) ? '' : '<img src="' + settings.icon + '" class="icon" />';
		var windowframe = '<div id="windowframe' + id + '" class="window" tabindex="' + id + '"><div class="window-header">' +
			icon + '<div class="title">' + settings.header + '</div>' + window_buttons + '</div>' +
			menu + '<div class="window-body"></div></div>';
		$('div.windows').append(windowframe);

		windowframe = $('div.windows div#' + _orb_window_id_label + id);
		windowframe.data('settings', settings);
		if (settings.dialog) {
			windowframe.addClass('dialog');
		}

		/* Menu
		 */
		windowframe.find('ul.nav a.entry').click(function(event) {
			settings.menuCallback(windowframe, $(this).text());
			windowframe.find($('ul.nav li').removeClass('open'));
			event.stopPropagation();
		});

		/* Right-click dummy
		 */
		$('div.windows div#' + _orb_window_id_label + id).contextmenu(function() {
			menu_entries = [{ name: 'About this application', icon: 'info-circle' }];
            orb_contextmenu_show($(this), event, menu_entries, orb_window_about);
			return false;
		});

		windowframe.find('span.glyphicon-remove').click(windowframe_close);
		windowframe.find('span.fa-window-maximize').click(windowframe_maximize);
		windowframe.find('span.fa-window-minimize').click(windowframe_minimize);

		/* Add body
		 */
		var body = element.detach();
		windowframe.find('div.window-body').append(body.show());

		/* Style
		 */
		if (settings.width > $(window).width()) {
			settings.width = $(window).width();
		}

		windowframe.css({
			display: 'none', position: 'absolute',
			boxShadow: '10px 10px 20px #181818',
			width: settings.width + 'px', zIndex: 1
		});
		if (settings.height != undefined) {
			windowframe.css({
				height: (settings.height + 50) + 'px'
			});
		}

		if (settings.bgcolor != undefined) {
			windowframe.css('background-color', settings.bgcolor);
		}

		/* Click
		 */
		windowframe.click(function(event) {
			orb_window_raise($(this));
		});

		/* Draggable
		 */
		windowframe.draggable({
			containment: 'div.windows',
			handle: 'div.window-header',
			start: function() {
				orb_window_raise($(this));
				orb_startmenu_close();
			},
			stop: function() {
				var pos = $(this).position();
				if (pos.left < 0) {
					$(this).css('left', '0px');
				}
				if (pos.top < 0) {
					$(this).css('top', '0px');
				}
			}
		});

		/* Resizable
		 */
		if (settings.resize !== false) {
			windowframe.resizable({
				minWidth: settings.minwidth,
				stop: function() {
					if ((settings.resize != undefined) && (settigs.resize != false)) {
						settings.resize();
					}
					windowframe.removeData('maximize');
				}
			});
		}

		/* Activator
		 */
		if (settings.activator != undefined) {
			$(settings.activator).attr('id', 'windowframe' + id);
			$(settings.activator).click(function() {
				element.open();
			});
		}

		if (settings.dialog == false) {
			orb_taskbar_add('windowframe' + id);
		}
	};

	/* Functions
	 */
	var unselect_text = function() {
		if (window.getSelection || document.getSelection) {
			window.getSelection().removeAllRanges();
		} else {
			document.selection.empty();
		}
	}

	var windowframe_open = function() {
		var windowframe_id = $(this).data('windowframe_id');
		var windowframe = $('div.windows div#' + _orb_window_id_label + windowframe_id);
		var settings = windowframe.data('settings');

		orb_window_raise(windowframe);

		windowframe.fadeIn(500, function() {
			if (settings.open != undefined) {
				settings.open();
			}
		});

		var mobile_device = $('div.desktop').attr('mobile') == 'yes';

		/* Center windowframe
		 */
		var left = Math.round((window.innerWidth / 2) - (settings.width / 2));
		if (left < 0) {
			left = 0;
		}
		if (mobile_device == false) {
			left += Math.floor((Math.random() * 50) - 25);
		}
		windowframe.css('left', left + 'px');

		var height = windowframe.outerHeight(false);
		if (settings.top == undefined) {
			var top = Math.round((window.innerHeight / 2.5) - (height / 2));
			if (top < 0) {
				top = 0;
			}
			if (mobile_device == false) {
				top += Math.floor((Math.random() * 50) - 25);
			}
			windowframe.css('top', top + 'px');
		} else {
			windowframe.css('top', settings.top);
		}

		var pos = windowframe.position();
		var bottom = pos.top + height;
		if (bottom > window.innerHeight - MARGIN_BOTTOM) {
			windowframe.find('div.window-body').css({
				maxHeight: (height - (bottom - window.innerHeight) - 45 - MARGIN_BOTTOM) + 'px',
				overflowY: 'auto'
			});
		}
	};

	var windowframe_maximize = function() {
		var windowframe_id = $(this).parent().parent().attr('id');
		orb_window_maximize(windowframe_id);
	}

	var windowframe_minimize = function() {
		var windowframe_id = $(this).parent().parent().attr('id');
		orb_window_minimize(windowframe_id);
	}

	var windowframe_close = function() {
		// close via javascript?
		var windowframe_id = $(this).attr('id');
		if (windowframe_id == undefined) {
			// close via window header close button?
			windowframe_id = $(this).parent().parent().attr('id');
		}

		if (windowframe_id != undefined) {
			var windowframe = $('div.windows div#' + windowframe_id);
			var settings = windowframe.data('settings');

			if (settings.close != undefined) {
				if (settings.close() === false) {
					return;
				}
			}

			if (settings.dialog) {
				var id = windowframe.find('div.window-body > div').data('windowframe_id');
				$('div.windows div.overlay' + id).remove();
				$('div.taskbar div.overlay' + id).remove();
			}

			windowframe.remove();
			orb_taskbar_remove(windowframe_id);

			delete $(this);
		} else if (confirm('Orb Error: Object has no window id. Remove anyway?')) {
			$('div.windows div.overlay').remove();
			$('div.taskbar div.overlay').remove();
			$(this).parent().parent().remove();
		}
	};

	var get_body = function() {
		var windowframe_id = $(this).data('windowframe_id');
		return $('div.windows div#' + _orb_window_id_label + windowframe_id + ' div.window-body').children().first();
	}

	/* JQuery prototype
	 */
	$.fn[pluginName] = function(options) {
		return this.each(function() {
			(new plugin(this, options));
		});
	};

	$.fn.extend({
		open: windowframe_open,
		close: windowframe_close,
		body: get_body
	});
})(jQuery);
