/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function dosbox_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return extension.toLowerCase() == 'jsdos';
}

function dosbox_start_bundle(dosbox_window, bundle = undefined) {
	dosbox_window.empty();

	var iframe = '<iframe src="/apps/dosbox/dosbox.php';
	if (bundle != undefined) {
		iframe += '?bundle=' + bundle;
	}
	iframe += '"></iframe>';

	dosbox_window.append(iframe);
}

function dosbox_open(filename = undefined) {
	var window_content =
		'<div class="dosbox"><p>Select File &Rightarrow; Open to open a .jsdos file.</p></div>';

	var dosbox_window = $(window_content).orb_window({
		header:'DosBox',
		icon: '/apps/dosbox/dosbox.png',
		width: 731,
		height: 400,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Download': [ 'Abandonware DOS', 'Classic Reload', 'DOS Games', 'DOS Games Archive', '-', 'Create DosBox file' ],
			'Help': [ 'About' ]
		},
		menuCallback: dosbox_menu_click,
	});

	dosbox_start_bundle(dosbox_window, filename);
	dosbox_window.open();
}

function dosbox_menu_click(dosbox_window, item) {
	dosbox_window = dosbox_window.find('div.dosbox');

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (dosbox_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					dosbox_start_bundle(dosbox_window, filename);
				}
			}, 'Emulators/DosBox');
			break;
		case 'Exit':
			dosbox_window.close();
			break;
		case 'Abandonware DOS':
			window.open('https://www.abandonwaredos.com/', '_blank');
			break;
		case 'Classic Reload':
			window.open('https://classicreload.com/', '_blank');
			break;
		case 'DOS Games':
			window.open('https://dosgames.com/', '_blank');
			break;
		case 'DOS Games Archive':
			window.open('https://www.dosgamesarchive.com/', '_blank');
			break;
		case 'Create DosBox file':
			window.open('https://dos.zone/studio/', '_blank');
			break;
		case 'About':
			orb_alert('DosBox emulator\nCopyright (c) by Hugo Leisink\n\njs-dos\nCopyright (c) by caiiiycuk\nhttps://js-dos.com/\n\nDOSBox\nCopyright (c) by the DOSBox crew\n<a href="https://www.dosbox.com/" target="_blank">https://www.dosbox.com/</a>', 'About');
			break;
	}
}

$(document).ready(function() {
	var icon = '/apps/dosbox/dosbox.png';
	orb_startmenu_add('DosBox', icon, dosbox_open);
	orb_upon_file_open('jsdos', dosbox_open, icon);
});
