<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	if (defined("ORB_VERSION") == false) exit;

	class explorer extends orb_backend {
		private $settings = null;

		/* Upload file
		 */
		private function directory_size($directory) {
			if (($dp = opendir($directory)) == false) {
				return false;
			}

			$result = 0;

			while (($file = readdir($dp)) != false) {
				if (($file == ".") || ($file == "..")) {
					continue;
				}

				$path = $directory."/".$file;
				if (is_dir($path)) {
					$result += $this->directory_size($path);
				} else if (($size = filesize($path)) !== false) {
					$result += $size;
				}
			}

			closedir($dp);

			return $result;
		}

		public function post() {
			if (is_true(READ_ONLY)) {
				$this->view->return_error(403);
				return;
			}

			if (is_array($_FILES["file"] ?? null) == false) {
				$this->view->return_error(400);
				return;
			}

			if ($_FILES["file"]["error"] != 0) {
				$this->view->return_error(400);
				return;
			}

			$path = trim($_POST["path"] ?? "", "/ ");
			if ($this->valid_filename($path) == false) {
				$this->view->return_error(400);
				return;
			}

			/* Check capacity
			 */
			$users = file(PASSWORD_FILE);

			$capacity = null;
			foreach ($users as $user) {
				$parts = explode(":", trim($user));

				if ($parts[0] != $this->username) {
					continue;
				} else if (count($parts) < 3) {
					continue;
				} else if ($parts[2] == "") {
					continue;
				}

				$capacity = (int)$parts[2];
			}

			if ($capacity !== null) {
				$total_size = $this->directory_size($this->home_directory);

				if ($total_size + (int)$_FILES["file"]["size"] > $capacity * MB) {
					$this->view->return_error(403);
					return;
				}
			}

			/* Move file
			 */
			$destination = $this->home_directory."/".$path;

			if (is_dir($destination) == false) {
				$this->view->return_error(404);
				return;
			}

			if ($path != "" ) {
				$destination .= "/";
			}
			$destination .= $_FILES["file"]["name"];

			move_uploaded_file($_FILES["file"]["tmp_name"], $destination);

			$this->view->add_tag("path", $path);
		}

		/* File share functions
		 */
		private function shares_load() {
			ob_start();
			$settings = file_get_contents($this->home_directory."/.settings");
			ob_end_clean();

			if ($settings == false) {
				return false;
			}

			$this->settings = json_decode($settings, true);
			$shares = $this->settings["applications"]["explorer"]["shares"] ?? null;

			if ($shares === null) {
				$shares = array();
			}

			return $shares;
		}

		private function shares_save($shares) {
			if ($this->settings === null) {
				$this->shares_load();
			}

			$this->settings["applications"]["explorer"]["shares"] = $shares;

			if (($fp = fopen($this->home_directory."/.settings", "w")) == false) {
				return false;
			}

			fputs($fp, json_encode($this->settings));

			fclose($fp);

			return true;
		}

		public function post_share() {
			if ($this->valid_filename($_POST["filename"]) == false) {
				$this->view->return_error(406);
				return;
			}

			if (email::valid_address($_POST["email"] ?? null) == false) {
				$this->view->return_error(406);
				return;
			}

			$expire = (int)($_POST["expire"] ?? 1);
			if (($expire < 1) || ($expire > 31)) {
				$expire = 1;
			}

			$expire = date("Y-m-d", strtotime("+".$expire." days"));

			/* Load shares
			 */
			if (($shares = $this->shares_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			/* New share
			 */
			do {
				$key = random_string(20);
			} while (isset($shares[$key]));

			$shares[$key] = $_POST["filename"]."|".$expire."|".$_POST["email"];

			/* Save shares
			 */
			if ($this->shares_save($shares) == false) {
				$this->view->return_error(500);
				return;
			}

			/* Mail share
			 */
			$link_base = "https://".$_SERVER["HTTP_HOST"]."/apps/explorer/share.php";
			$link_params = "user=".$this->username."&share=".$key;

			$username = strtoupper(substr($this->username, 0, 1)).substr($this->username, 1);
			$expire = date("j F Y", strtotime($expire));

			$message = file_get_contents(__DIR__."/share.txt");
			$fields = array(
				"FILE"     => basename($_POST["filename"]),
				"COMMENT"  => $_POST["comment"],
				"EXPIRE"   => $expire,
				"LINK"     => $link_base."?".$link_params,
				"USERNAME" => $username);

			$email = new email("Orb file share", "no-reply@".$_SERVER["HTTP_HOST"]);
			$email->set_message_fields($fields);
			$email->message($message);
			$email->send($_POST["email"]);
		}

		public function get_shares() {
			/* Load shares
			 */
			if (($shares = $this->shares_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			$changed = false;

			foreach ($shares as $key => $share) {
				$parts = explode("|", $share);
				$email = array_pop($parts);
				$expire = array_pop($parts);

				$timestamp = strtotime($expire);
				if (time() >= $timestamp) {
					unset($shares[$key]);
					$changed = true;
					continue;
				}

				$expire = date("j F Y", $timestamp);
				$file = implode("|", $parts);

				$this->view->open_tag("share", array("key" => $key));
				$this->view->add_tag("file", $file);
				$this->view->add_tag("expire", $expire);
				$this->view->add_tag("email", $email);
				$this->view->close_tag();
			}

			if ($changed == false) {
				return;
			}

			$this->shares_save($shares);
		}

		public function post_unshare() {
			/* Load shares
			 */
			if (($shares = $this->shares_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			if (isset($shares[$_POST["key"]]) == false) {
				$this->view->return_error(404);
				return;
			}

			unset($shares[$_POST["key"]]);

			/* Save shares
			 */
			if ($this->shares_save($shares) == false) {
				$this->view->return_error(500);
				return;
			}
		}
	}
?>
