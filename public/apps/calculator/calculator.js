/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function calculator_calculate(value1, operator, value2) {
	value1 = parseFloat(value1);
	value2 = parseFloat(value2);

	if (isNaN(value1) || isNaN(value2)) {
		return undefined;
	}

	switch (operator) {
		case '+':
			return value1 + value2;
		case '-':
			return value1 - value2;
		case '/':
			if (value2 == 0) {
				return undefined;
			} else {
				return value1 / value2;
			}
		case 'x':
			return value1 * value2;
		case '^':
			return Math.pow(value1, value2);
		case '%':
			return value1 % value2;
	}

	return undefined;
}

function calculator_click(button) {
	var value = $(button.target).text();
	var display = $(button.target).parent().parent().find('input#display');

	var operators = [ '+', '-', '/', 'x', '^', '%' ];
	if (operators.includes(value)) {
		if ((display.data('operator') != undefined) && (operators.includes(display.data('previous')) == false)) {
			var value1 = display.data('value');
			var value2 = display.val();

			var result = calculator_calculate(value1, display.data('operator'), value2);
			if (result == undefined) {
				display.data('value', '0');
				display.data('operator', null);
				display.data('newvalue', true);
				display.val('error');

				return;
			}

			display.val(result);
		}

		if (display.val() != '') {
			display.data('value', display.val());
		}
		display.data('operator', value);
		display.data('newvalue', true);
	} else switch (value) {
		case '=':
			if (display.data('operator') != undefined) {
				var value1 = display.data('value');
				var value2 = display.val();

				if (display.data('previous') == '=') {
					var value3 = value2;
					value2 = value1;
					value1 = value3;
				} else {
					display.data('value', value2);
				}

				value = calculator_calculate(value1, display.data('operator'), value2);
				if (value == undefined) {
					value = 'error';
					display.data('value', '0');
					display.data('operator', null);
					display.data('newvalue', true);
				}

				display.val(value);
			} else {
				value = display.val();
			}

			orb_setting_set('applications/calculator/display', value);

			display.data('newvalue', true);
			break;
		case 'AC':
			display.val('');
			display.data('value', '0');
			display.data('operator', null);
			display.data('newvalue', false);
			orb_setting_set('applications/calculator/display', '');
			break;
		case 'C':
			display.val('');
			break;
		case '.':
			var current = display.val();
			if (current.includes(value) == false) {
				display.val(current + value);
			}
			break;
		default:
			if (display.data('previous') == '=') {
				display.data('operator', null);
			}

			if (display.data('newvalue') == false) {
				var current = display.val();
				if (current.length >= 20) {
					return;
				}
				value = current + value;
			}

			display.val(value);
			display.data('newvalue', false);
	}

	display.data('previous', $(button.target).text());

	display.focus();
}

function calculator_open() {
	var buttons = [ [ '7', '8', '9', 'C', 'AC' ],
	                [ '4', '5', '6', '+', 'x' ],
	                [ '1', '2', '3', '-', '/' ],
	                [ '0', '.', '^', '%', '='  ] ];
	var window_content = '<div class="calculator"><input type="text" readonly="readonly" id="display" class="form-control" />';
	buttons.forEach(function(row) {
		window_content += "<div>";
		row.forEach(function(button) {
			window_content += '<button>' + button + '</button>';
		});
		window_content += "</div>";
	});
	window_content += '</div>';

	var calculator_window = $(window_content).orb_window({
		header:'Calculator',
		icon: '/apps/calculator/calculator.png',
		width: 280,
		height: 249,
		resize: false,
		maximize: false
	});

	calculator_window.find('button').click(calculator_click);

	var display = calculator_window.find('input#display');

	display.data('value', '0');
	display.data('operator', null);
	display.data('newvalue', false);
	display.data('previous', false);

	display.keypress(function(event) {
		var key = event.originalEvent.key;

		switch (key) {
			case 'Enter': key = '='; break;
			case 'c': key = 'C'; break;
			case 'A': key = 'AC'; break;
			case 'a': key = 'AC'; break;
			case 'X': key = 'x'; break;
			case '*': key = 'x'; break;
		}

		calculator_window.find('button').each(function() {
			if ($(this).text() == key) {
				$(this).click();
				return false;
			}
		});
	});

	calculator_window.parent().click(function() {
		display.focus();
	});

	orb_setting_get('applications/calculator/display', function(value) {
		display.val(value);
		display.data('newvalue', true);
	});

	calculator_window.open();

	display.focus();
}

$(document).ready(function() {
	orb_startmenu_add('Calculator', '/apps/calculator/calculator.png', calculator_open);
});
