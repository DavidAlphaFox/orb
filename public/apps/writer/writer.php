<?php
	if (defined("ORB_VERSION") == false) exit;

	class writer extends orb_backend {
		public function post_pdf() {
			if (empty($_POST["filename"])) {
				$this->view->return_error(400);
				return;
			} else if ($this->valid_filename($_POST["filename"]) == false) {
				$this->view->return_error(400);
				return;
			}

			require "vendor/autoload.php";

			$style = file_get_contents("apps/writer/ckstyles.css");
			$html = '<html><head><style type="text/css">'.$style.'</style></head><body class="ck-content">'.$_POST["content"]."</body></html>";

			$dompdf = new Dompdf\Dompdf();
			$dompdf->loadHtml($html);
			$dompdf->setPaper("A4", "portrait");
			$dompdf->render();

			$filename = $this->home_directory."/".$_POST["filename"];

			if (file_exists($filename)) {
				unlink($filename);
			}

			file_put_contents($filename, $dompdf->output());
		}
	}
?>
