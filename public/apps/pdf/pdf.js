/* Orb pdf application
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications,
 *
 * Always use pdf_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

function pdf_open_file(pdf_window, filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	if (extension.toLowerCase() != 'pdf') {
		orb_alert('Not a PDF file.');
		return;
	}

	pdf_window.find('iframe').attr('src', orb_download_url(filename));

	pdf_window.data('filename', filename);
}

function pdf_menu_click(pdf_window, item) {
	pdf_window = pdf_window.find('div.pdf');

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				pdf_open_file(pdf_window, filename);
			}, 'Documents');
			break;
		case 'Download':
			var filename = pdf_window.data('filename');
			if (filename == undefined) {
				return;
			}

			var url = orb_download_url(filename);
			window.open(url, '_blank').focus();
			break;
		case 'Exit':
			pdf_window.close();
			break;
		case 'About':
			orb_alert('PDF viewer\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function pdf_open(filename = undefined) {
	var window_content =
		'<div class="pdf"><iframe></iframe></div>';

	var pdf_window = $(window_content).orb_window({
		header:'PDF',
		icon: '/apps/pdf/pdf.png',
		width: 500,
		height: 600,
		menu: {
			'File': [ 'Open', 'Download', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: pdf_menu_click
	});

	pdf_window.open();

	if (filename != undefined) {
		pdf_open_file(pdf_window, filename);
	}
}

$(document).ready(function() {
    var icon = '/apps/pdf/pdf.png';
    orb_startmenu_add('PDF', icon, pdf_open);
    orb_upon_file_open('pdf', pdf_open, icon);
});
