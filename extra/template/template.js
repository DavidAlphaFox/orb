/* Orb template application
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications,
 *
 * Always use template_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

function template_menu_click(template_window, item) {
	template_window = template_window.find('div.template');

	switch (item) {
		case 'Exit':
			template_window.close();
			break;
		case 'About':
			orb_alert('Template\nCopyright (c) by NAME', 'About');
			break;
	}
}

function template_open(filename = undefined) {
	var window_content =
		'<div class="template">' +
		'<p>This is the Orb Template application.</p>' +
		'</div>';

	var template_window = $(window_content).orb_window({
		header:'Template',
		icon: '/images/application.png',
		width: 400,
		height: 200,
		menu: {
			'File': [ 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: template_menu_click
	});

	template_window.open();

	if (filename != undefined) {
	}
}

$(document).ready(function() {
	orb_startmenu_add('Template', '/images/application.png', template_open);
});
