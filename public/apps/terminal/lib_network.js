/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_dns(term, parameters) {
	if (terminal_argument_count(term, parameters, 1) == false) {
		return false;
	}
	
	host = parameters[0];

	$.ajax({
		url: '/terminal/dns/' + host
	}).done(function(data) {
		if ($(data).find('result *').length > 0) {
			const records = {
				A: 'ip',
				AAAA: 'ipv6',
				TXT: 'txt',
				CAA: 'value',
				SOA: 'rname'
			};

			$(data).find('result').each(function() {
				var type = $(this).find('type').text();

				term.write(type.padEnd(10, ' ') + ' ');

				if ((type = records[type]) == undefined) {
					type = 'target';
				}

				term.write($(this).find(type).text().padEnd(20, ' ') + ' ');
				term.writeln('');
			});
		} else {
			term.writeln($(data).find('result').text());
		}
		terminal_done(term);
	}).fail(function() {
		term.writeln('Invalid hostname or IP address.');
		terminal_done(term);
	});
}

function terminal_ping(term, parameters) {
	if (terminal_argument_count(term, parameters, 1) == false) {
		return false;
	}

	host = parameters[0];

	$.ajax({
		url: '/terminal/ping/' + host
	}).done(function(data) {
		$(data).find('line').each(function() {
			term.writeln($(this).text());
		});
		terminal_done(term);
	}).fail(function() {
		term.writeln('Invalid hostname or IP address.');
		terminal_done(term);
	});
}

function terminal_port(term, parameters) {
	if (terminal_argument_count(term, parameters, 2) == false) {
		return false;
	}

	host = parameters[0];
	port = parameters[1];

	$.ajax({
		url: '/terminal/port/' + host + '/' + port
	}).done(function(data) {
		term.writeln('Port is open.');
		terminal_done(term);
	}).fail(function(result) {
		if (result.status == 404) {
			term.writeln('Invalid hostname or IP address.');
		} else {
			term.writeln('Port is closed.');
		}
		terminal_done(term);
	});
}
