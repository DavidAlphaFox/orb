Installation
============

Copy Orb to a suitable location. Make the directory 'public' the document root
in your web server configuration. Let your web server rewrite every request to
a non-existing file to /index.php.

Orb needs PHP with the XSL module. 

Make sure the files in the logfiles directory are writable by your web server.


Authentication
--------------

Orb supports two authentication methods. HTTP authentication and Orb's own
login form. Set AUTHENTICATION in orb.conf as desired.

When using Orb's login form, Orb expects the usernames and passwords to be
stored in a file named users.txt located in the same directory as where the
home directories are. You can change this location via the orb.conf file.
You can use the script 'setup' to create a new account.


Home directories
----------------

Each user needs a home directory. Home directories must be created in the
directory as specified in orb.conf. A user's home directory must have the same
name as the username of that user. Copy extra/user.settings to that home
directory and rename it to .settings. You can use the script 'setup' to create
a new home directory.

Make sure the user's home directory and everything in it, including the
settings file, are readable and writable by the web server.


User websites
-------------

Each user can have a static website. Files that are placed in the directory
Website are available via the URL https://<your Orb hostname>/~<username>/.
When a directory is requested, Orb will look for the file index.html.


For developers
--------------

As Orb is still work in progress, documentation is in early stages. At this
moment, the only documentation is extra/API.txt.

For now, you can use the existing applications (see public/apps) as an example.

You can use the script 'setup' to create a new application.
