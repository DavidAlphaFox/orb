/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const TERMINAL_PROMPT = '\x1B[1m>\x1B[0m ';
const TERMINAL_PROMPT_LENGTH = 2;

function terminal_argument_count(term, parameters, count) {
    if (parameters.length < count) {
        term.writeln('Arguments missing.');
        terminal_done(term);
        return false;
    }

	if (parameters.length > count) {
        term.writeln('Too many arguments.');
        terminal_done(term);
        return false;
    }

	return true;
}

function terminal_combine_directories(base, dir) {
	if (dir.substr(0, 1) == '/') {
		return dir.substr(1);
	}

	if (base == '') {
		var base_parts = []
	} else {
		var base_parts = base.split('/');
	}

	var dir_parts = dir.split('/');

	dir_parts.forEach(function(part) {
		if (part == '..') {
			base_parts.pop();
		} else if (part != '.') {
			base_parts.push(part);
		}
	});

	return base_parts.join('/');
}

function terminal_done(term) {
	if (term.busy == false) {
		return;
	}

	if (term.script == false) {
		term.writeln('\x1B[1m/' + term.path + '\x1B[0m');
		term.write(TERMINAL_PROMPT);
	}

	term.command = '';
	term.busy = false;
}

function terminal_open(filename = undefined) {
	var window_content =
		'<div class="terminal"></div>';

	var terminal_window = $(window_content).orb_window({
		header:'Terminal',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 730,
		height: 465,
		resize: false,
		maximize: false
	});

	orb_load_stylesheet('/apps/terminal/xterm.css');
	orb_load_javascript('/apps/terminal/xterm.js');

	orb_load_javascript('/apps/terminal/lib_change_dir.js');
	orb_load_javascript('/apps/terminal/lib_command.js');
	orb_load_javascript('/apps/terminal/lib_complete_input.js');
	orb_load_javascript('/apps/terminal/lib_directory.js');
	orb_load_javascript('/apps/terminal/lib_file.js');
	orb_load_javascript('/apps/terminal/lib_hacking.js');
	orb_load_javascript('/apps/terminal/lib_hiawatha.js');
	orb_load_javascript('/apps/terminal/lib_keypress.js');
	orb_load_javascript('/apps/terminal/lib_network.js');
	orb_load_javascript('/apps/terminal/lib_list.js');
	orb_load_javascript('/apps/terminal/lib_system.js');
	orb_load_javascript('/apps/terminal/lib_help.js');

	terminal_window.open();

	var term = new Terminal({
		cursorBlink: 'block'
	});

	term.open(terminal_window[0]);

	/* Size
	 */
	var width = terminal_window.find('div.xterm-screen').width();
	var height = terminal_window.find('div.xterm-screen').height();

	terminal_window.css('width', width + 'px');
	terminal_window.css('height', height + 'px');

	var windowframe = terminal_window.parent().parent();
	windowframe.css('width', (width + 20) + 'px');
	windowframe.css('height', (height + 50) + 'px');

	/* Start
	 */
	term.writeln('  \x1B[1;37m--==[ \x1B[34mOrb Terminal\x1B[1;37m ]==--\x1B[0m\n');
	term.focus();

	term.history = [ '' ];
	term.history_pointer = 0;

	if (filename != undefined) {
		/* Execute script
		 */
		orb_load_javascript('/apps/terminal/lib_script.js');

		term.script = true;
		term.path = orb_file_dirname(filename);
		term.busy = false;

		orb_file_open(filename, function(content) {
			var lines = content.split('\n');

			var script = [];
			lines.forEach(function(line) {
				if (line.substr(0, 1) != '#') {
					script.push(line);
				}
			});

			terminal_run_script(terminal_window, term, script);
		});
	} else {
		/* Start interactive shell
		 */
		term.writeln('  Type \'help\' for a list of supported commands.\n');

		term.script = false;
		term.path = '';

		terminal_done(term);

		term.onKey(function(event) {
			terminal_keypress(terminal_window, term, event);
			return false;
		});

		/* Copy
		 */
		term.onSelectionChange(function(event) {
			document.execCommand('copy');
		});

		/* Paste
		 */
		terminal_window.find('textarea').on('paste', function(event) {
			var text = event.originalEvent.clipboardData.getData('text');

			term.command += text;
			term.write(text);
		});
	}
}

$(document).ready(function() {
	var icon = '/apps/terminal/terminal.png';
	orb_startmenu_add('Terminal', icon, terminal_open);
	orb_upon_file_open('ots', terminal_open, icon);

	if (orb_get_cookie('autoexec') == 'yes') {
		document.cookie = 'autoexec=no';
		var run_autoexec = true;
	} else if ($('div.desktop').attr('login') == 'none') {
		var run_autoexec = true;
	} else {
		var run_autoexec = false;
	}

	if (run_autoexec) {
		document.cookie = 'autoexec=no';

		orb_file_exists('autoexec.ots', function(exists) {
			if (exists) {
				terminal_open('autoexec.ots');
			}
		});
	}
});
