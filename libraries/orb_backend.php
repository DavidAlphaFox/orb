<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	abstract class orb_backend {
		protected $view = null;
		protected $username = null;
		protected $home_directory = null;
		protected $parameters = array();
		protected $get_filename = null;

		/* Constructor
		 *
		 * INPUT:  object view, string username
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($view, $username) {
			$this->view = $view;
			$this->username = $username;
			$this->view->mode = "xml";

			if (substr(HOME_DIRECTORIES, 0, 1) == "/") {
				$this->home_directory = HOME_DIRECTORIES;
			} else {
				$separator = (PHP_OS_FAMILY == "Windows") ? "\\" : "/";
				$parts = explode($separator, __DIR__);
				array_pop($parts);
				$this->home_directory = implode("/", $parts)."/".HOME_DIRECTORIES;
			}

			$this->home_directory .= "/".$username;
		}

		/* Check filename validity
		 *
		 * INPUT:  string filename
		 * OUTPUT: boolean result
		 * ERROR:  -
		 */
		protected function valid_filename($filename) {
			if (strpos($filename, "\x00") !== false) {
				return false;
			}

			if (substr($filename, 0, 2) == "..") {
				return false;
			}

			if (strpos($filename, "/.") !== false) {
				return false;
			}

			return true;
		}

		/* Check if directory is crucial to Orb
		 */
		protected function is_system_directory($directory) {
			static $directories = null;

			if ($directories === null) {
				$directories = array($this->home_directory);
				foreach (SYSTEM_DIRECTORIES as $sys_dir) {
					array_push($directories, $this->home_directory."/".$sys_dir);
				}
			}

			return in_array($directory, $directories);
		}

		/* Default execute function
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function execute() {
			$method = strtolower($_SERVER["REQUEST_METHOD"]);
			if (($pos = strpos($_SERVER["REQUEST_URI"], "?")) !== false) {
				$_SERVER["REQUEST_URI"] = substr($_SERVER["REQUEST_URI"], 0, $pos);
			}
			$parts = explode("/", trim($_SERVER["REQUEST_URI"], "/"));
			$this->parameters = array();

			if ($parts[0] == "orb") {
				array_shift($parts);
			}
			if (get_class($this) != "orb") {
				array_shift($parts);
			}
			array_unshift($parts, $method);

			/* Set parameters
			 */
			while (count($parts) > 0) {
				$function = implode("_", $parts);

				if (method_exists($this, $function)) {
					/* Set filename
					 */
					$this->get_filename = $this->home_directory;
					if (count($this->parameters) > 0) {
						$filename = "/".urldecode(implode("/", $this->parameters));
						if ($this->valid_filename($filename) == false) {
							$this->view->return_error(400);
							return false;
						}

						$this->get_filename .= $filename;
					}

					/* Get POST content
					 */
					if (($_SERVER["REQUEST_METHOD"] == "POST") && (($_SERVER["HTTP_CONTENT_TYPE"] ?? null) == "application/octet-stream")) {
						$_POST = file_get_contents("php://input");
					}

					/* Execute requested function
					 */
					if (call_user_func(array($this, $function)) === false) {
						$this->view->return_error(500);
					}

					return;
				}

				$part = array_pop($parts);
				array_unshift($this->parameters, $part);
			}

			/* Return error
			 */
			$methods = array_diff(array("GET", "POST", "PUT", "DELETE"), array($_SERVER["REQUEST_METHOD"]));
			$allowed = array();
			foreach ($methods as $method) {
				if (method_exists($this, strtolower($method))) {
					array_push($allowed, $method);
				}
			}

			if (count($allowed) == 0) {
				$this->view->return_error(404);
			} else {
				$this->view->return_error(405);
				header("Allowed: ".implode(", ", $allowed));
			}
		}
	}
?>
