<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	define("ORB_VERSION", "0.16");
	define("DEFAULT_COLOR", "#286090");
	define("DESKTOP_PATH", "Desktop");
	define("EDITOR", "notepad_open");
	define("SYSTEM_DIRECTORIES", array(DESKTOP_PATH, "Shared", "Temporary"));
	define("TERMINAL_NETWORK_TIMEOUT", 5);
	define("NONE_AUTH_HOMEDIR", "public");

	if (substr(HOME_DIRECTORIES, 0, 1) == "/") {
		define("PASSWORD_FILE", HOME_DIRECTORIES."/users.txt");
	} else {
		define("PASSWORD_FILE", "../".HOME_DIRECTORIES."/users.txt");
	}

	/* Scan for applications
	 */
	$apps = array();

	if (($dp = opendir("apps")) != false) {
		while (($app = readdir($dp)) != false) {
			if (substr($app, 0, 1) == ".") {
				continue;
			}

			if (file_exists("apps/".$app."/".$app.".js") == false) {
				continue;
			}

			array_push($apps, $app);
		}

		closedir($dp);
	}

	define("APPLICATIONS", $apps);

	function orb_application_exists($application) {
		return in_array($application, APPLICATIONS);
	}

	/* Orb system backend
	 */
	class orb extends orb_backend {
		public function get_ping() {
			$this->view->add_tag("pong");
		}
	}
?>
