<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	if (defined("ORB_VERSION") == false) exit;

	class settings extends orb_backend {
		public function get_color() {
			$this->view->add_tag("color", DEFAULT_COLOR);
		}

		public function get_userinfo() {
			$this->view->add_tag("username", $this->username);

			$users = file(PASSWORD_FILE);

			$capacity = "unlimited";
			foreach ($users as $user) {
				$parts = explode(":", trim($user));

				if ($parts[0] != $this->username) {
					continue;
				} else if (count($parts) < 3) {
					continue;
				} else if ($parts[2] == "") {
					continue;
				}

				$capacity = (int)$parts[2]." MB";
			}

			$this->view->add_tag("capacity", $capacity);
		}

		public function post_password() {
			$passwords = file(PASSWORD_FILE);

			foreach ($passwords as $i => $line) {
				$parts = explode(":", trim($line));

				if ($parts[0] != $this->username) {
					continue;
				}

				if (password_verify($_POST["current"], $parts[1]) == false) {
					$this->view->return_error(403);
					$this->view->add_tag("result", "Current password incorrect.");
					return;
				}

				if (trim($_POST["newpwd"]) == "") {
					$this->view->return_error(403);
					$this->view->add_tag("result", "New password cannot be empty.");
					return;
				}

				if ($_POST["newpwd"] != $_POST["repeat"]) {
					$this->view->return_error(403);
					$this->view->add_tag("result", "New password not repeated correctly.");
					return;
				}

				$parts[1] = password_hash($_POST["newpwd"], PASSWORD_ARGON2I);

				$passwords[$i] = implode(":", $parts)."\n";

				if (($fp = fopen(PASSWORD_FILE, "w")) == false) {
					$this->view->return_error(403);
					$this->view->add_tag("result", "Password file not writable.");
					return;
				}

				foreach ($passwords as $line) {
					fputs($fp, $line);
				}
				fclose($fp);

				break;
			}
		}
	}
?>
