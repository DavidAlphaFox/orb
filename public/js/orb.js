const KEY_SHIFT = 16;
const KEY_CTRL = 17;

var _orb_setting_error_shown = false;
var _orb_file_icons = [];
var _orb_callbacks_open_file = {};
var _orb_callback_open_directory = undefined;
var _orb_keys_down = {};
var _orb_icon_context_menu = {};

/* Alert
 */
function orb_alert(message, title = '') {
	message = message.replaceAll('\n', '<br />');

	var dialog =
		'<div class="orb_alert">' +
		'<div class="message">' + message + '</div>' + 
		'<div class="btn-group">' +
		'<input type="button" value="Ok" class="btn btn-default" />' +
		'</div>' +
		'</div>';
	var alert_window = $(dialog).orb_window({
		header: title,
		width: 500,
		maximize: false,
		minimize: false,
		resize: false,
		dialog: true
	});

	alert_window.find('div.btn-group input').click(function() {
		alert_window.close();
	});

	alert_window.open();
}

/* Context menu
 */
function orb_contextmenu_add_items(menu_entries, extension) {
	var items = _orb_icon_context_menu[extension];
	if (items == undefined) {
		return;
	}

	menu_entries.push('-');
	items.forEach(function(item) {
		menu_entries.push({ name: item.label, icon: item.icon, callback:item.callback });
	});
}

function orb_contextmenu_show(icon, event, menu_entries, callback) {
	var menu_x = event.clientX;
	var menu_y = event.clientY;
	var z_index = orb_window_max_zindex() + 2;

	var menu = $('<div class="context_menu" style="position:absolute; display:none; z-index:' + z_index + ';">');
	menu_entries.forEach(function(value) {
		if (value == '-') {
			menu.append('<div><hr /></div>');
		} else {
			var item = $('<div class="option"><span class="fa fa-' + value.icon + '"></span><span class="text">' + value.name + '</span></div>');

			if (value.callback != undefined) {
				var cb = value.callback;
			} else {
				var cb = callback;
			}

			item.on('mousedown', function() {
				$('body div.context_menu').remove();
				cb(icon, value.name);
			});

			menu.append(item);
		}
	});

	$('body div.context_menu').remove();
	$('body').append(menu);

	var desktop_width = Math.round($('div.desktop').width());
	var desktop_height = Math.round($('div.desktop').height());

	var menu = $('div.context_menu');
	var menu_width = Math.round(menu.outerWidth());
	var menu_height = Math.round(menu.outerHeight());

	if (menu_x + menu_width > desktop_width) {
		menu_x -= menu_width;
	}

	if (menu_y + menu_height > desktop_height) {
		menu_y -= menu_height;
	}

	menu.css('left', menu_x + 'px');
	menu.css('top', menu_y + 'px');
	menu.css('display', '');

	$(document).one('mousedown', function() {
		$('body div.context_menu').remove();
	});
}

function orb_contextmenu_extra_item(extension, label, icon, callback) {
	var entry = {
		label: label,
		icon: icon,
		callback: callback
	}

	if (_orb_icon_context_menu[extension] == undefined) {
		_orb_icon_context_menu[extension] = [];
	}

	_orb_icon_context_menu[extension].push(entry);
}

/* Icon functions
 */
function orb_icon_to_filename(icon) {
	var container = $(icon).parent();

	if (container.hasClass('icons') && container.parent().hasClass('desktop')) {
		/* Desktop
		 */
		return 'Desktop/' + $(icon).find('span').text();
	}

	if (container.hasClass('files') && container.parent().hasClass('explorer')) {
		/* Explorer
		 */
		var explorer_window = container.parent();
		var path = explorer_window.data('path');
		return path + '/' + $(icon).find('span').first().text();
	}

	return undefined;
}

function orb_make_icon(name, image) {
	return '<div class="icon">' +
		'<img src="' + image + '" alt="' + name + '" title="' + name + '" draggable="false" />' +
		'<span>' + name + '</span></div>';
}

function orb_get_file_icon(extension) {
	if (typeof extension === 'string') {
		extension = extension.toLowerCase();
	}

	var default_icon = '/images/file.png';
	var handler = _orb_callbacks_open_file[extension];

	if (_orb_file_icons.includes(extension)) {
		default_icon = '/images/icons/' + extension + '.png';
	}

	if (handler == undefined) {
		return default_icon;
	}

	if (handler.icon == undefined) {
		return default_icon;
	}

	return handler.icon;
}

/* File and directory handlers
 */
function orb_upon_file_open(extension, callback, icon = undefined) {
	if (typeof extension === 'string') {
		extension = extension.toLowerCase();
	}

	var handler = {
		callback: callback,
		icon: icon
	}

	if (_orb_callbacks_open_file[extension] == undefined) {
		_orb_callbacks_open_file[extension] = handler;
	} else {
		orb_alert('Duplicate extension handler for .' + extension + ' files.', 'Orb error');
	}
}

function orb_upon_directory_open(callback) {
	if (_orb_callback_open_directory == undefined) {
		_orb_callback_open_directory = callback;
	}
}

function orb_get_file_handler(extension) {
	if (typeof extension === 'string') {
		extension = extension.toLowerCase();
	}

	var handler = _orb_callbacks_open_file[extension];

	if (handler == undefined) {
		return undefined;
	}

	return handler.callback;
}

function orb_get_directory_handler(extension) {
	return _orb_callback_open_directory;
}

/* Cookie
 */
function orb_get_cookie(cookie) {
	var parts = document.cookie.split(';');

	var cookies = {};
	parts.forEach(function(part) {
		var item = part.split('=');
		var key = item[0].trim();
		var value = item[1].trim();

		cookies[key] = value;
	});

	return cookies[cookie];
}

/* Settings
 */
function orb_setting_get(setting, callback_done, callback_fail = undefined) {
	$.ajax({
		url: '/orb/setting/' + setting
	}).done(function(data) {
		var result = $(data).find('result').text();
		callback_done(result);
	}).fail(function(result) {
		if ((result.status == 500) && (_orb_setting_error_shown == false)) {
			_orb_setting_error_shown = true;
			orb_alert('User settings file not found. Read INSTALL for instructions.', 'Orb error');
		}

		if (callback_fail != undefined) {
			callback_fail(result.status);
		}
	});
}

function orb_setting_set(setting, value, callback_done = undefined, callback_fail = undefined) {
	$.post('/orb/setting/' + setting, {
		value: value
	}).done(function() {
		if (callback_done != undefined) {
			callback_done();
		}
	}).fail(function(result) {
		if ((result.status == 500) && (_orb_setting_error_shown == false)) {
			_orb_setting_error_shown = true;
			orb_alert('User settings file not writable for webserver.', 'Orb error');
		}

		if (callback_fail != undefined) {
			callback_fail(result.status);
		}
	});
}

/* Dynamically add resources
 */
function orb_load_javascript(javascript) {
	if ($('head script[src="' + javascript + '"]').length > 0) {
		return;
	}

	javascript += '?' + Date.now();

	$('head').append('<script type="text/javascript" src="' + javascript + '"></script>');
}

function orb_load_stylesheet(stylesheet) {
	if ($('head link[href="' + stylesheet + '"]').length > 0) {
		return;
	}

	stylesheet += '?' + Date.now();

	$('head').append('<link rel="stylesheet" type="text/css" href="' + stylesheet + '" />');
}

/* Logout
 */
function orb_logout() {
	if ($('div.windows div.window').length > 0) {
		if (confirm('Close all windows and logout?') == false) {
			return;
		}
	}

	var login = $('div.desktop').attr('login');

	var logout = window.location.protocol + '//';

	if (login == 'http') {
		logout += 'log:out@';
	}

	logout += window.location.hostname;

	if (login == 'orb') {
		logout += '/?logout';
	}

	$('body').empty().css('background-color', '#202020');
	window.location = logout;
}

/* Key press
 */
function orb_key_pressed(key) {
	return _orb_keys_down[key];
}

/* Main
 */
$(document).ready(function() {
	$.ajax({
		url: '/orb/icon/default'
	}).done(function(data) {
		$(data).find('icon').each(function() {
			_orb_file_icons.push($(this).text());
		});
	}).fail(function() {
		orb_alert('Error loading custom icons.', 'Orb error');
	});

	/* Register ctrl press
	 */
	var keys_init = function() {
		_orb_keys_down[KEY_SHIFT] = false;
		_orb_keys_down[KEY_CTRL] = false;
	};

	$(window).focus(keys_init);
	keys_init();

	$('body').keydown(function(event) {
		if (_orb_keys_down[event.which] !== undefined) {
			_orb_keys_down[event.which] = true;
		}
	});

	$('body').keyup(function(event) {
		if (_orb_keys_down[event.which] !== undefined) {
			_orb_keys_down[event.which] = false;
		}
	});

	/* Keep session alive
	 */
	var timeout = $('div.desktop').attr('timeout');
	if (timeout != undefined) {
		timeout = parseInt(timeout);
		if (isNaN(timeout)) {
			orb_alert('Invalid session timeout.', 'Orb error');
		} else {
			timeout = (timeout - 10) * 1000;
			setInterval(function() {
				$.ajax({
					url: '/orb/ping'
				}).fail(function(result) {
					if (result.status == 401) {
						orb_logout();
					}
				});
			}, timeout);
		}
	}
});
