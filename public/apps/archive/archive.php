<?php
	if (defined("ORB_VERSION") == false) exit;

	const VALID_EXTENSIONS = array("zip", "tgz");

	interface archiver_interface {
		public function open($archive);
		public function list();
		public function extract($file, $directory);
	}

	abstract class archiver_library {
		protected function list_add(&$list, $name, $size) {
			if (is_array($name) == false) {
				$name = explode("/", $name);
			}

			if (count($name) == 1) {
				$list[$name[0]] = array("size" => $size);
			} else {
				$dir = $name[0];

				if ($name[1] == "") {
					$list[$dir] = array("content" => array());
				} else {
					array_shift($name);
					$this->list_add($list[$dir]["content"], $name, $size);
				}
			}
		}
	}

	class archive extends orb_backend {
		private function get_archive($filename) {
			if (($pos = strrpos($filename, ".")) === false) {
				return false;
			}

			$archiver = strtolower(substr($filename, $pos + 1));

			if (in_array($archiver, VALID_EXTENSIONS) == false) {
				return false;
			}

			require($archiver.".php");

			$archive = new $archiver;
			$archive->open($filename);

			return $archive;
		}

		private function show_list($list, $name) {
			$this->view->open_tag("directory", array("name" => $name));

			foreach ($list as $name => $info) {
				if (isset($info["content"])) {
					$this->show_list($info["content"], $name);
				}
			}

			foreach ($list as $name => $info) {
				if (isset($info["size"])) {
					$this->view->add_tag("file", $name, array("size" => $info["size"]));
				}
			}

			$this->view->close_tag();
		}

		public function get_list() {
			if (($archive = $this->get_archive($this->get_filename)) == false) {
				$this->view->return_error(404);
				return;
			}

			$this->show_list($archive->list(), urldecode(implode("/", $this->parameters)));
		}

		public function post_extract() {
			if (empty($_POST["archive"])) {
				$this->view->return_error(400);
				return;
			} else if ($this->valid_filename($_POST["archive"]) == false) {
				$this->view->return_error(403);
				return;
			}

			if (empty($_POST["filename"])) {
				$this->view->return_error(400);
				return;
			} else if ($this->valid_filename($_POST["filename"]) == false) {
				$this->view->return_error(403);
				return;
			}

			if (isset($_POST["filename"]) == false) {
				$this->view->return_error(400);
				return;
			} else if ($this->valid_filename($_POST["directory"]) == false) {
				$this->view->return_error(403);
				return;
			}

			$directory = $this->home_directory."/".$_POST["directory"];
			if (is_dir($directory) == false) {
				$this->view->return_error(406);
				return;
			}

			$zipfile = $this->home_directory."/".$_POST["archive"];
			if (($archive = $this->get_archive($zipfile)) == false) {
				$this->view->return_error(404);
				return;
			}

			if (($target = $archive->extract($_POST["filename"], $directory)) == false) {
				$this->view->return_error(500);
				return;
			}

			$target = ltrim(substr($target, strlen($this->home_directory)), "/");

			$this->view->add_tag("filename", $target);
		}
	}
?>
