<?php
	/* Authentication
	 */
	require "../../../libraries/error.php";
	require "../../../libraries/general.php";

	session_name("Orb");
	session_start();

	$login = new login(null);
	if ($login->valid() == false) {
		return;
	}

	unset($_COOKIE["Orb"]);

	/* Browser
	 */
	require "http.php";
	require "proxy.php";

	if (isset($_GET["url"]) == false) {
		return;
	}

	$parts = explode("/", $_GET["url"], 4);
	$protocol = array_shift($parts);
	array_shift($parts);
	$hostname = array_shift($parts) ?? "";
	$path = array_shift($parts) ?? "";
	$path = "/".str_replace(" ", "%20", $path);

	$parts = explode(":", $hostname, 2);
	$hostname = array_shift($parts);
	$port = array_shift($parts);

	/* Start proxy
	 */
	if ($protocol == "http:") {
		$proxy = new proxy($hostname, $port);
	} else if ($protocol == "https:") {
		$proxy = new proxys($hostname, $port);
	} else {
		return;
	}

	/* Forward request
	 */
	$result = $proxy->forward_request($path);

	switch ($result) {
		case CONNECTION_ERROR:
			print "Connection error.";
			break;
		case LOOPBACK:
			print "Loopback to Orb is not allowed.";
			break;
	}
?>
