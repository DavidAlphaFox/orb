<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	class desktop {
		private $view = null;
		private $username = null;

		public function __construct($view, $username) {
			$this->view = $view;
			$this->username = $username;
		}

		/* Get request handler
		 *
		 * INPUT:  -
		 * OUTPUT: object request handler
		 * ERROR:  false
		 */
		private function get_request_handler() {
			$parts = explode("?", $_SERVER["REQUEST_URI"], 2);
			$request_uri = array_shift($parts);
			$parameters = array_shift($parts);

			$parts = explode("/", $request_uri);
			$name = $parts[1];

			if ($name == "") {
				return count($parts) > 2 ? false : null;
			}

			if ($name == "orb") {
				/* Orb system call
				 */
				$name = $parts[2];

				if ($name == "") {
					return false;
				}

				if (class_exists($name)) {
					if (is_subclass_of($name, "orb_backend")) {
						return new $name($this->view, $this->username);
					}
				}

				return new orb($this->view, $this->username);
			}

			/* Application backend call
			 */
			if (in_array($name, APPLICATIONS) == false) {
				return false;
			}

			$library = "apps/".$name."/".$name.".php";

			if (file_exists($library)) {
				ob_start();
				require_once $library;
				ob_end_clean();
			}

			if (class_exists($name) == false) {
				return false;
			}

			if (is_subclass_of($name, "orb_backend") == false) {
				return false;
			}

			return new $name($this->view, $this->username);
		}

		/* Show desktop
		 *
		 * INPUT:  -
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function show() {
			$core_parts = array("orb", "desktop", "windows", "taskbar", "file", "directory");

			/* Stylesheets
			 */
			$this->view->add_css("jquery-ui.css");
			$this->view->add_css("bootstrap.css");
			$this->view->add_css("bootstrap-theme.css");
			$this->view->add_css("font-awesome.css");
			foreach ($core_parts as $part) {
				$this->view->add_css($part.".css");
			}

			/* Javascripts
			 */
			$this->view->add_javascript("jquery.js");
			$this->view->add_javascript("jquery-ui.js");
			if ($this->view->mobile_device) {
				$this->view->add_javascript("jquery.ui.touch-punch.js");
			}
			$this->view->add_javascript("bootstrap.js");
			$this->view->add_javascript("library.js");

			foreach (APPLICATIONS as $application) {
				$this->view->add_application($application);
			}

			foreach ($core_parts as $part) {
				$this->view->add_javascript($part.".js");
			}

			/* Login information
			 */
			$this->view->open_tag("login");
			$this->view->add_tag("username", $this->username);
			$this->view->add_tag("method", AUTHENTICATION);
			if (AUTHENTICATION != "http") {
				$this->view->add_tag("timeout", ini_get("session.gc_maxlifetime"));
			}
			$this->view->close_tag();

			/* Home directory and desktop
			 */
			if (substr(HOME_DIRECTORIES, 0, 1) == "/") {
				$homedir = HOME_DIRECTORIES;
			} else {
				$homedir = __DIR__."/../".HOME_DIRECTORIES;
			}
			$homedir .= "/".$this->username;

			/* Load settings
			 */
			ob_start();
			$settings = file_get_contents($homedir."/.settings");
			ob_end_clean();

			if ($settings !== false) {
				$settings = json_decode($settings, true);
			} else {
				$settings = array("system" => array("zoom" => 0.75));
			}

			/* Create desktop
			 */
			$this->view->open_tag("desktop", array(
				"path"     => DESKTOP_PATH,
				"mobile"   => show_boolean($this->view->mobile_device),
				"zoom"     => $settings["system"]["zoom"],
				"editor"   => EDITOR,
				"readonly" => show_boolean(READ_ONLY)));
			$this->view->close_tag();

			return true;
		}

		public function execute() {
			$request_handler = $this->get_request_handler();

			if ($request_handler === null) {
				/* Desktop
				 */
				$xslt_file = "desktop";

				if ($this->show() == false) {
					$this->view->return_error(500);
				}
			} else if ($request_handler === false) {
				/* Error
				 */
				if ($this->view->ajax_request == false) {
					$this->view->add_css("bootstrap.css");
					$this->view->add_css("bootstrap-theme.css");
					$this->view->add_css("notification.css");

					$xslt_file = "error";
				}

				ob_get_clean();

				header("Status: 404");
				print "File not found.";
			} else {
				/* Application backend requests
				 */
				if (is_true(DEBUG_MODE) && ($_SERVER["REQUEST_METHOD"] == "POST")) {
					debug_log($_POST);
				}

				$request_handler->execute();
			}

			return $xslt_file ?? null;
		}
	}
?>
