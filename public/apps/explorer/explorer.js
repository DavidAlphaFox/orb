/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function explorer_set_path(explorer_window, path) {
	explorer_window.find('form input[name=path]').val(path);

	var target = explorer_window.find('div.path');

	target.empty();
	target.append('<a href="">/</a>');

	if (path != '') {
		var parts = path.split('/');
		var url = [];
		parts.forEach(function(part) {
			url.push(part);
			part += '/';
			target.append('<a href="' + url.join('/') + '">' + part + '</a>');
		});
	}

	target.find('a').click(function() {
		explorer_window.data('path', $(this).attr('href'));
		explorer_update(explorer_window);
		return false;
	});
}

function explorer_contextmenu_handler(icon, option) {
	var explorer_window = icon.parent().parent();
	var path = explorer_window.data('path');
	var filename = icon.find('span').first().text();

	switch (option) {
		case 'Download':
			var url = path;
			if (path != '') {
				url += '/';
			}
			url += filename;
			url = orb_download_url(url);

			window.open(url, '_blank').focus();
			break;
		case 'Rename':
			var new_filename = prompt('Rename file', filename);
			if (new_filename !== null) {
				orb_file_rename(path + '/' + filename, new_filename, undefined, function() {
					orb_alert('Error while renaming file or directory.', 'Error');
				});
			}
			break;
		case 'Create link':
			orb_file_dialog('Link', function(link) {
				orb_file_link(path + '/' + filename, link, undefined, function() {
					orb_alert('Error creating link.', 'Error');
				});
			}, 'Desktop', filename);
			break;
		case 'Link info':
			var target = icon.find('span').first().attr('target');
			orb_alert('Link to: ' + target);
			break;
		case 'Share':
			var share = filename;
			if (path != '') {
				share = path + '/' + share;
			}

			var dialog = '<div class="explorer_share">' +
				'<div class="form-group">' +
				'<label>Share:</label>' +
				'<div>' + share + '</div>' +
				'</div><div class="form-group">' +
				'<label for="email">E-mail address:</label>' +
				'<input type="text" id="email" class="form-control" />' +
				'</div><div class="form-group">' +
				'<label for="expire">Expire after:</label>' +
				'<select id="expire" class="form-control">' +
				'<option value="1">This day</option>' +
				'<option value="3">Three days</option>' +
				'<option value="7">One week</option>' +
				'<option value="14">Two weeks</option>' +
				'<option value="31">One month</option>' +
				'</select>' +
				'</div><div class="form-group">' +
				'<label for="comment">Comment:</label>' +
				'<input type="text" id="comment" class="form-control" />' +
				'</div><div class="btn-group">' +
				'<button class="btn btn-default share">Share</button>' +
				'<button class="btn btn-default cancel">Cancel</button>' +
				'</div></div>';
			var share_window = $(dialog).orb_window({
				header: 'File sharing',
				icon: '/apps/explorer/explorer.png',
				width: 400,
				dialog: true,
				maximize: false,
				resize: false,
				open: function() {
					share_window.find('input#email').focus();
				}
			});

			share_window.find('button.share').click(function(event) {
				share_window.find('.has-error').removeClass('has-error');

				var email = share_window.find('input#email').val().trim().toLowerCase();
    			if (email.match(/^(.+)@(.+)\.([a-z]{2,})$/) == null) {
					share_window.find('input#email').parent().addClass('has-error');
					share_window.find('input#email').focus();
					return;
				}

				$.post('/explorer/share', {
					filename: path + '/' + filename,
					email: share_window.find('input#email').val(),
					expire: share_window.find('select#expire').val(),
					comment: share_window.find('input#comment').val()
				}).done(function() {
					share_window.close();
				}).fail(function(result) {
					orb_alert('Error sharing file.');
				});
				event.stopPropagation();
			});

			share_window.find('button.cancel').click(function() {
				share_window.close();
			});

			share_window.open();
			break;
		case 'Delete':
			if (confirm('Delete ' + filename + '?')) {
				if (icon.attr('type') == 'file') {
					orb_file_remove(path + '/' + filename, undefined, function() {
						orb_alert('Error while deleting file.', 'Error');
					});
				} else {
					orb_directory_remove(path + '/' + filename, undefined, function() {
						orb_alert('Error while deleting directory.', 'Error');
					});
				}
			}
			break;
	}
}

/* File icon
 */
function explorer_entry_add(files, format, item, path) {
	if (item.type == 'directory') {
		var image = '/images/directory.png';
		extension = '';
	} else {
		var extension = orb_file_extension(item.name);
		if (extension != false) {
			var image = orb_get_file_icon(extension);
		} else {
			var image = '/images/file.png';
			extension = '';
		}
	}

	var icon = '<div class="entry ' + format + '" type="' + item.type + '" ext="' + extension + '" link="' + (item.link ? 'yes' : 'no') + '">' +
		'<img src="' + image + '" alt="' + item.name + '" draggable="false" />' +
		(item.link ? '<img src="/images/link.png" draggable="false" />' : '') +
		'<span path="' + path + '" type="' + item.type + '"';
	if (item.create != undefined) {
		icon += ' create="' + item.create + '"';
	}
	if (item.access != undefined) {
		icon += ' access="' + item.access + '"';
	}
	if (item.target != undefined) {
		icon += ' target="' + item.target + '"';
	}
	icon += '>' + item.name + '</span>';
	if ((item.type == 'file') && (item.size != undefined)) {
		icon += '<span number="' + item.size + '">';
		icon += orb_file_nice_size(item.size);
	} else {
		icon += '<span>&nbsp;';
	}

	if ($('div.desktop').attr('mobile') == 'yes') {
		icon += '</span><span class="glyphicon glyphicon-chevron-down" aria-hidden="true">';
	}

	icon += '</span></div>';

	files.append(icon);
}

function explorer_update(explorer_window) {
	var path = explorer_window.data('path');

	explorer_set_path(explorer_window, path);

	orb_directory_list(path, function(items) {
		var information = explorer_window.find('div.information');
		var files = explorer_window.find('div.files');
		var filename = explorer_window.find('input.filename');
		var format = explorer_window.data('format');

		information.empty();
		files.empty();
		filename.val('');

		/* Fill explorer
		 */
		items.forEach(function(item) {
			if (item.type == 'directory') {
				explorer_entry_add(files, format, item, path);
			}
		});
		var dir_count = files.find('div').length;

		items.forEach(function(item) {
			if (item.type == 'file') {
				explorer_entry_add(files, format, item, path);
			}
		});
		var file_count = files.find('div').length - dir_count;

		information.append('<b style="display:block; margin-top:30px">Content:</b>');
		information.append('<p>' + dir_count + ' directories<br />' + file_count + ' files</p>');

		/* Mobile
		 */
		if ($('div.desktop').attr('mobile') == 'yes') {
			explorer_window.find('div.files div.detail span:nth-of-type(2)').css('right', '25px');
			explorer_window.find('div.files div.detail span:nth-of-type(3)').css('display', 'inline');
			explorer_window.find('div.files div.detail span:nth-of-type(3)').click(function(event) {
				var e = jQuery.Event('contextmenu');
				e.clientX = event.clientX;
				e.clientY = event.clientY;
				$(this).parent().trigger(e);
				event.stopPropagation();
			});
		}

		/* Double-click file
		 */
		explorer_window.find('div.files div.entry[type=directory]').dblclick(function() {
			var dir = $(this).find('span').first().text();

			if (path == '') {
				path = dir;
			} else {
				path += '/' + dir;
			}

			explorer_window.data('path', path);
			explorer_update(explorer_window);
		});

		/* Double-click file
		 */
		explorer_window.find('div.files div.entry[type=file]').dblclick(function() {
			var filename = path + '/' + $(this).find('span').first().text();
			var extension = orb_file_extension(filename);

			if ((handler = orb_get_file_handler(extension)) != undefined) {
				handler(filename);
			} else {
				window.open(orb_download_url(filename), '_blank').focus();
			}
		});

		/* Click file
		 */
		explorer_window.find('div.files div.entry').click(function() {
			var name = $(this).find('span').first().text();

			if ($('div.desktop').attr('mobile') == 'yes') {
				if (explorer_window.data('click_last') == name) {
					explorer_window.data('click_last', null);
					$(this).trigger('dblclick');
				} else {
					explorer_window.data('click_last', name);
				}
			}

			explorer_window.find('div.files div.selected').removeClass('selected');
			$(this).addClass('selected');

			var information = explorer_window.find('div.information');
			information.empty();

			var src = $(this).find('img').attr('src');
			information.append('<img src="' + src + '" />');

			information.append('<p><b>File name:</b><br />' + name + '</p>');

			var create = $(this).find('span').first().attr('create');
			if ((create != undefined) && (create != '')) {
				var parts = create.split(', ');
				information.append('<p><b>Creation date:</b><br />' + parts[0] + '<br />' + parts[1] + '</p>');
			}

			var access = $(this).find('span').first().attr('access');
			if ((access != undefined) && (access != '')) {
				var parts = access.split(', ');
				information.append('<p><b>Last access date:</b><br />' + parts[0] + '<br />' + parts[1] + '</p>');
			}

			var type = $(this).find('span').first().attr('type');
			if (type == 'file') {
				var size = $(this).find('span').last().attr('number');
				if (size != null) {
					information.append('<p><b>File size:</b><br />' + size + '</p>');
				}
			}
		});

		/* Right-click file
		 */
		var window_id = explorer_window.parent().parent().attr('id');

		$('div#' + window_id + ' div.files div.entry').contextmenu(function(event) {
			var menu_entries = [];

			if ($(this).attr('type') == 'file') {
				menu_entries.push({ name: 'Download', icon: 'download' });
			}

			if (orb_read_only == false) {
				menu_entries.push({ name: 'Rename', icon: 'edit' });

				if ($(this).attr('link') == 'yes') {
					menu_entries.push({ name: 'Link info', icon: 'info-circle' });
				} else {
					menu_entries.push({ name: 'Create link', icon: 'chain' });
				}
			}

			var login = $('div.desktop').attr('login');
			if (login != 'none') {
				menu_entries.push({ name: 'Share', icon: 'upload' });
			}

			if (orb_read_only == false) {
				menu_entries.push({ name: 'Delete', icon: 'remove' });
			}

			if (menu_entries.length > 0) {
				orb_contextmenu_add_items(menu_entries, $(this).attr('ext'));
				orb_contextmenu_show($(this), event, menu_entries, explorer_contextmenu_handler);
			}
			return false;
		});

		if (orb_read_only == false) {
			explorer_window.find('div.files > div.entry').draggable({
				appendTo: 'div.icons',
				helper: 'clone',
				zIndex: 10000,
				start: function(event, ui) {
					if (ui.helper.hasClass('detail')) {
						ui.helper.removeClass('detail');
						ui.helper.addClass('icon');
					}
				}
			});
		}

		if (($('div.desktop').attr('mobile') == 'yes') && ($('input.drag').prop('checked') == false)) {
			explorer_window.find('div.files > div.entry').draggable({
				disabled: true
			});
		}
	}, function(result) {
		if (result == 401) {
			explorer_window.close();
			alert('Login has been expired. No access to disk.');
			orb_logout();
		} else if (path != '') {
			orb_alert('Error reading directory.', 'Error');
			explorer_window.data('path', '');
			explorer_update(explorer_window);
		}
	});
}

function explorer_set_format(explorer_window, format) {
	if (explorer_window.data('format') != format) {
		explorer_window.data('format', format);
		orb_setting_set('applications/explorer/format', format);
		explorer_update(explorer_window);
	}
}

function explorer_menu_click(explorer_window, item) {
	explorer_window = explorer_window.find('div.explorer');

	switch (item) {
		case 'New Explorer window':
			var path = explorer_window.data('path');
			explorer_open(path);
			break;
		case 'New directory':
			var directory = prompt('New directory name');

			if (directory == null) {
				break;
			} else if (directory.trim() == '') {
				break;
			} else if (directory.indexOf('/') != -1) {
				break;
			}

			var path = explorer_window.data('path');
			orb_directory_create(path + '/' + directory, undefined, function() {
				orb_alert('Error while creating directory.', 'Error');
			});
			break;
		case 'New file':
			var file = prompt('New file name');

			if (file == null) {
				break;
			} else if (file.trim() == '') {
				break;
			} else if (file.indexOf('/') != -1) {
				break;
			}

			var path = explorer_window.data('path');
			orb_file_save(path + '/' + file, "", false, undefined, function() {
				orb_alert('Error while creating file.', 'Error');
			});
			break;
		case 'Icons':
			explorer_set_format(explorer_window, 'icon');
			break;
		case 'Details':
			explorer_set_format(explorer_window, 'detail');
			break;
		case 'File shares':
			$.ajax({
				url: '/explorer/shares'
			}).done(function(data) {
				var shares = '<div class="explorer_shares"><table class="table table-condensed table-striped">' +
					'<thead><tr><th>File</th><th>Expires</th><th>Shared with</th><th></th></tr></thead>' +
					'<tbody>';
				$(data).find('share').each(function() {
					shares += '<tr key="' + $(this).attr('key') + '">' +
						'<td>' + $(this).find('file').text() + '</td>' +
						'<td>' + $(this).find('expire').text() + '</td>' +
						'<td>' + $(this).find('email').text() + '</td>' +
						'<td><button class="btn btn-primary btn-xs">Delete</button></tr>';
				});
				shares += '</tbody></table></div>';

				var shares_window = $(shares).orb_window({
					header: 'File shares',
					icon: '/apps/explorer/explorer.png',
					width: 700,
					dialog: true,
					maximize: false,
					resize: false
				});

				shares_window.find('button.btn').click(function() {
					var share = $(this).parent().parent();

					$.post('/explorer/unshare',	{
						key: share.attr('key')
					}).done(function() {
						share.remove();
					}).fail(function() {
						orb_alert('Error unsharing file.');
					});
				});

				shares_window.open();
			}).fail(function(result) {
				orb_alert('Error loading shares.');
			});
			break;
		case 'Exit':
			explorer_window.close();
			break;
		case 'About':
			orb_alert('File explorer\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function explorer_open(directory = undefined) {
	var dropzone_id = 0
	while ($('div.explorer form#dropzone' + dropzone_id).length > 0) {
		dropzone_id++;
	}

	var window_content =
		'<div class="explorer">' +
		'<button class="btn btn-default btn-xs refresh"><span class="fa fa-refresh"></span></button>' +
		'<button class="btn btn-default btn-xs directory_up"><span class="fa fa-chevron-up"></span></button>' +
		'<div class="path"></div>' +
		'<div class="information"></div>' +
		'<div class="files"></div>' +
		'<form action="/explorer" class="dropzone" id="dropzone' + dropzone_id + '">' +
		'<div class="dz-message" data-dz-message><span>Drop files here to upload them to the current folder.</span></div>' +
		'<input type="hidden" name="path" value="" />' +
		'</form>' +
		'</div>';
	
	if (orb_read_only) {
		var menu_file = [ 'New Explorer window', 'Exit' ];
	} else {
		var menu_file = [ 'New Explorer window', 'New directory', 'New file', '-', 'Exit' ];
	}

	var explorer_window = $(window_content).orb_window({
		header:'Explorer',
		icon: '/apps/explorer/explorer.png',
		width: 760,
		height: 430,
		menu: {
			'File': menu_file,
			'View': [ 'Icons', 'Details', '-', 'File shares' ],
			'Help': [ 'About' ]
		},
		menuCallback: explorer_menu_click
	});

	explorer_window.parent().parent().css('min-height', '270px');

	explorer_window.open();

	if (directory == undefined) {
		directory = '';
	}

	explorer_window.data('path', directory);

	orb_setting_get('applications/explorer/format', function(value) {
		explorer_window.data('format', value);
		explorer_update(explorer_window);
	}, function() {
		explorer_window.data('format', 'icon');
		explorer_update(explorer_window);
		orb_setting_set('applications/explorer/format', 'icon');
	});

	explorer_window.find('button.directory_up').click(function() {
		var path = explorer_window.data('path');

		if (path == '') {
			return;
		}

		var parts = path.split('/');
		parts.pop();
		path = parts.join('/');

		explorer_window.data('path', path);
		explorer_update(explorer_window);
	});

	explorer_window.find('button.refresh').click(function() {
		explorer_update(explorer_window);
	});

	if (orb_read_only == false) {
		explorer_window.droppable({
			accept: 'div.icon, div.detail',
			greedy: true,
			over: function() {
				var current = $(this);
				$('div.windows div.explorer').each(function() {
					if ($(this).is(current) == false) {
						$(this).droppable('disable');
					}
				});

				orb_window_raise(explorer_window.parent().parent());
			},
			out: function() {
				$('div.windows div.explorer').droppable('enable');
			},
			drop: function(event, ui) {
				event.stopPropagation();

				var span = ui.helper.find('span').first();
				var source_path = span.attr('path');
				var source = source_path + '/' + span.text();
				var destination_path = explorer_window.data('path');
				var source_filename = orb_file_filename(source);

				if (source_path == destination_path) {	
					if (source_path == 'Desktop') {
						orb_desktop_refresh();
					}
					return;
				}

				orb_file_exists(destination_path + '/' + source_filename, function(exists) {
					if (exists) {
						if (confirm('Destination file already exists. Overwrite?') == false) {
							return;
						}
					}

					if (orb_key_pressed(KEY_CTRL)) {
						orb_file_copy(source, destination_path, undefined, function() {
							orb_alert('Error copying file.', 'Error');
						});
					} else {
						orb_file_move(source, destination_path, undefined, function() {
							orb_alert('Error moving file.', 'Error');
						});
					}
				});

				$('div.windows div.explorer').droppable('enable');
			}
		});

		/* Dropzone
		 */
		var dropzone_visible = false;
		var files_dropped = 0;

		Dropzone.options['dropzone' + dropzone_id] = {
			init: function() {
				this.on('drop', function(data) {
					files_dropped += data.dataTransfer.files.length;
				});

				this.on('success', function(file) {
					var path = explorer_window.data('path');
					orb_directory_notify_update(path);
				});

				this.on('complete', function(file) {
					files_dropped--;

					var dropzone = this;
					setTimeout(function() {
						dropzone.removeFile(file)

						if (files_dropped == 0) {
							explorer_window.find('form.dropzone').hide();
							dropzone_visible = false;
						}
					}, 3000);
				});

				this.on('error', function(file, data) {
					if (data.substr(0, 5) == '<?xml') {
						var error = parseInt($(data).find('error').text());
						switch (error) {
							case 400:
								orb_alert('Error uploading file.');
								break;
							case 403:
								orb_alert('The maximum of your storage capacity has been reached.');
								break;
							case 404:
								orb_alert('Error uploading file to the specified destination.');
								break;
						}
					} else {
						orb_alert(data);
					}
				});
			}
		};

		var dropzone = new Dropzone('#dropzone' + dropzone_id, {
			url: '/explorer'
		});

		explorer_window.on('dragover', function(event) {
			event.preventDefault();
			event.stopPropagation();

			if (dropzone_visible == false) {
				dropzone_visible = true;
				explorer_window.find('form.dropzone').show();

				setTimeout(function() {
					if (files_dropped == 0) {
						explorer_window.find('form.dropzone').hide();
						dropzone_visible = false;
					}
				}, 3000);
			}
		});
	}

	/* Mobile support
	 */
	if ($('div.desktop').attr('mobile') == 'yes') {
		explorer_window.find('div.information').css('width', '150px');
		explorer_window.find('div.information').css('font-size', '12px');
		explorer_window.find('div.files').css('left', '165px');

		explorer_window.find('form.dropzone').css('bottom', '30px');

		explorer_window.find('div.information').css('bottom', '25px');
		if (orb_read_only == false) {
			var drag = $('<div class="drag"><input type="checkbox" class="drag" />Icons draggable</div>');
			drag.find('input').click(function() {
				var checked = $(this).prop('checked');
				explorer_window.find('div.files > div.entry').draggable({
					disabled: checked == false
				});
			});
			explorer_window.append(drag);
		}

		explorer_window.find('div.files').css('bottom', '30px');
		var scroll = $('<div class="scroll btn-group"><button class="btn btn-default btn-xs scroll_up"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></button><button class="btn btn-default btn-xs scroll_down"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>');
		scroll.find('button.scroll_up').click(function() {
			var current = explorer_window.find('div.files').scrollTop();
			explorer_window.find('div.files').scrollTop(current - 88);
		});
		scroll.find('button.scroll_down').click(function() {
			var current = explorer_window.find('div.files').scrollTop();
			explorer_window.find('div.files').scrollTop(current + 88);
		});
		explorer_window.append(scroll);
	}
}

$(document).ready(function() {
	orb_startmenu_add('Explorer', '/apps/explorer/explorer.png', explorer_open);
	orb_quickstart_add('Explorer', '/apps/explorer/explorer.png', explorer_open);
	orb_upon_directory_open(explorer_open);

	orb_directory_upon_update(function(directory) {
		$('div.windows div.explorer').each(function() {
			if ($(this).data('path') == directory) {
				explorer_update($(this));
			}
		});
	});

	if ($('div.desktop').attr('read_only') == 'no') {
		orb_load_javascript('/apps/explorer/dropzone.js');
		orb_load_stylesheet('/apps/explorer/dropzone.css');
	}
});
