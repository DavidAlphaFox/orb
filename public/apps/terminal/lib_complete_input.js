/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_complete_input(term) {
	var escaped = false;
	var quoted = false;
	var split = 0;
	for (i = split; i < term.command.length; i++) {
		var chr = term.command.substr(i, 1);

		if ((chr == '\\') && (escaped == false)) {
			escaped = true;
			continue;
		} else if ((chr == '\'') && (escaped == false)) {
			quoted = (quoted == false);
			split = i + 1;
		} else if ((chr == ' ') && (quoted == false)) {
			split = i + 1;
		}

		escaped = false;
	}

	if (escaped) {
		return;
	}

	var rest = term.command.substr(0, split);
	var parameter = term.command.substr(split);
	var parts = parameter.split('/');
	var file = parts.pop().replace('\\\'', '\'');

	if ((parts.length == 1) && (parts[0] == '')) {
		parameter = '/';
	} else {
		parameter = parts.join('/');
	}

	var directory = term.path;
	directory = terminal_combine_directories(directory, parameter);

	term.busy = true;

	orb_directory_list(directory, function(items) {
		var match = null;
		var matches = [];
		items.forEach(function(item) {
			if (item.name.substr(0, file.length) == file) {
				if (item.type == 'file') {
					matches.push(item.name);
				} else {
					matches.push(item.name + '/');
				}

				if (file == '') {
					return;
				}

				if (match == null) {
					match = item.name;
				} else {
					var length = item.name.length;
					if (match.length < length) {
						length = match.length;
					}

					for (var i = length; i > 0; i--) {
						if (match.substr(0, i) == item.name.substr(0, i)) {
							match = match.substr(0, i);
							break;
						}
					}
				}
			}
		});

		if (matches.length > 1) {
			term.writeln('');
			matches.forEach(function(item) {
				term.writeln(item);
			});

			if (match == null) {
				term.write('\r' + TERMINAL_PROMPT + term.command);
			}
		} else if ((matches.length == 1) && (match == null)) {
			match = matches[0];
		}

		if (match != null) {
			term.command = rest;

			match = match.replace('\'', '\\\'');

			if (parts.length > 0) {
				match = parts.join('/').replace('\'', '\\\'') + '/' + match
			}

			if ((match.indexOf(' ') != -1) && (quoted == false)) {
				match = '\'' + match;
				quoted = true;
			}

			term.command += match;

			if ((matches.length == 1) && quoted) {
				term.command += '\'';
			}
			term.write('\r' + TERMINAL_PROMPT + term.command);
		}

		term.busy = false;
	}, function() {
		term.busy = false;
	});
}
