<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	class login {
		private $view = null;

		/* Constructor
		 *
		 * INPUT:  object view
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($view) {
			$this->view = $view;
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			if ($key == "username") {
				switch (AUTHENTICATION) {
					case "http": return $_SERVER["REMOTE_USER"];
					case "orb": return $_SESSION["username"];
					case "none": return NONE_AUTH_HOMEDIR;
				}
			}

			return null;
		}

		/* Check username validity
		 *
		 * INPUT:  string username
		 * OUTPUT: bool username validity
		 * ERROR:  -
		 */
		private function valid_username($username) {
			if (strlen($username) == 0) {
				return false;
			}

			if (ctype_lower($username) == false) {
				return false;
			}

			return true;
		}

		/* Check login
		 *
		 * INPUT:  -
		 * OUTPUT: bool login valid
		 * ERROR:  -
		 */
		public function valid() {
			if (AUTHENTICATION == "none") {
				return true;
			}

			if (AUTHENTICATION == "http") {
				/* HTTP authentication
				 */
				if ($_SERVER["REMOTE_USER"] == null) {
					print "Enable HTTP authentication in your web server.";
					return false;
				}

				if ($this->valid_username($_SERVER["REMOTE_USER"]) == false) {
					print "Invalid username. Only lower-case letters are allowed.";
					return false;
				}

				return true;
			}

			if (AUTHENTICATION != "orb") {
				print "Invalid authentication method. Change it on orb.conf.";
				return false;
			}

			/* Orb authentication
			*/
			if (isset($_GET["logout"])) {
				$logfile = new logfile("orb");
				$logfile->user_id = $_SESSION["username"];
				$logfile->add_entry("user logged out");

				$_SESSION["username"] = null;

				return false;
			}

			if (isset($_SESSION["username"])) {
				return true;
			}

			return false;
		}

		/* Show login form necessities
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function form_necessities() {
			header("Status: 401");

			$this->view->add_css("bootstrap.css");
			$this->view->add_css("bootstrap-theme.css");
			$this->view->add_css("notification.css");
			$this->view->add_css("login.css");

			$this->view->add_javascript("jquery.js");
			$this->view->add_javascript("login.js");
		}

		/* Delete directory content
		 *
		 * INPUT:  string path
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function empty_directory($directory) {
			if (($dp = opendir($directory)) == false) {
				return;
			}

			while (($file = readdir($dp)) != false) {
				if (substr($file, 0, 1) == ".") {	
					continue;
				}

				$file = $directory."/".$file;
				
				if (is_dir($file)) {
					$this->empty_directory($file);
					rmdir($file);
				} else {
					unlink($file);
				}
			}

			closedir($dp);
		}

		/* Validate login
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function validate() {
			if (($users = file(PASSWORD_FILE)) === false) {
				$this->view->return_error(500);
				return;
			}

			$logfile = new logfile("orb");

			foreach ($users as $user) {
				list($username, $password) = explode(":", trim($user));

				if ($this->valid_username($username) == false) {
					continue;
				}

				if ($username != $_POST["username"]) {
					continue;
				}

				if (password_verify($_POST["password"], $password)) {
					$_SESSION["username"] = $username;
					if (orb_application_exists("terminal")) {
						setcookie("autoexec", "yes");
					}

					$logfile->user_id = $_POST["username"];
					$logfile->add_entry("user logged in");

					/* Empty directory Temporary
					 */
					if (substr(HOME_DIRECTORIES, 0, 1) == "/") {
						$homedir = HOME_DIRECTORIES;
					} else {
						$homedir = __DIR__."/../".HOME_DIRECTORIES;
					}

					$this->empty_directory($homedir."/".$username."/Temporary");

					return;
				}
			}

			$logfile->add_entry("Invalid login: %s", $_POST["username"]);

			$this->view->return_error(401);
		}

		/* Main
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$this->validate();
			} else {
				$this->form_necessities();
			}

			return "login";
		}
	}
?>
