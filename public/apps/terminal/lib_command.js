/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var terminal_applications = null;

function terminal_split_parameters(line) {
	var parameters = [ '' ];
	var p = 0;
	var escaped = false;
	var quoted = false;

	for (var i = 0; i < line.length; i++) {
		var chr = line.substr(i, 1);

		if ((chr == ' ') && (quoted == false)) {
			if (parameters[p].trim() != '') {
				parameters[++p] = '';
			}
		} else if ((chr == '\\') && (escaped == false)) {
			escaped = true;
			continue;
		} else if ((chr == '\'') && (escaped == false)) {
			if (quoted || (parameters[p].trim() != '')) {
				p++;
			}
			parameters[p] = '';

			quoted = (quoted == false);
		} else {
			parameters[p] += chr;
		}

		escaped = false;
	}

	if (quoted || escaped) {
		return false;
	}

	if (parameters[p] == '') {
		parameters.pop();
	}

	return parameters;
}

function terminal_command(terminal_window, term) {
	term.busy = true;

	var parts = term.command.trim().split(' ');
	var command = parts.shift();

	if (parts.length > 0) {
		var parameters = parts.join(' ').trim();
		parameters = terminal_split_parameters(parameters);

		if (parameters === false) {
			term.writeln('Syntax error.');
			terminal_done(term);
			return;
		}
	} else {
		var parameters = [];
	}

	if (term.script) {
		var script_command = true;

		switch (command) {
			case 'anykey':
				terminal_anykey(terminal_window, term, parameters);
				break;
			case 'echo':
				terminal_echo(term, parameters);
				break;
			case 'minimize':
				terminal_minimize(terminal_window, term);
				break;
			default:
				script_command = false;
		}

		if (script_command) {
			return;
		}
	}

	switch (command) {
		case '':
			terminal_done(term);
			break;
		case 'cat':
			terminal_file_cat(term, parameters);
			break;
		case 'cd':
			terminal_change_dir(term, parameters);
			break;
		case 'clear':
			term.reset();
			term.write('\x9B1A');
			terminal_done(term);
			break;
		case 'color':
			terminal_setting_color(term, parameters);
			break;
		case 'cp':
			terminal_file_copy(term, parameters);
			break;
		case 'dns':
			terminal_dns(term, parameters);
			break;
		case 'edit':
			terminal_file_edit(term, parameters);
			break;
		case 'exit':
			terminal_window.close();
			break;
		case 'hack':
			terminal_hack(term, parameters);
			break;
		case 'help':
			terminal_show_help(term, parameters);
			break;
		case 'kill':
			terminal_kill(term, parameters);
			break;
		case 'ls':
			terminal_list(term, parameters);
			break;
		case 'mkdir':
			terminal_directory_create(term, parameters);
			break;
		case 'mv':
			terminal_file_move(term, parameters);
			break;
		case 'open':
			terminal_file_open(term, parameters);
			break;
		case 'password':
			terminal_password(term, parameters);
			break;
		case 'ping':
			terminal_ping(term, parameters);
			break;
		case 'port':
			terminal_port(term, parameters);
			break;
		case 'ps':
			terminal_processes(term, parameters);
			break;
		case 'rm':
			terminal_file_remove(term, parameters);
			break;
		case 'rmdir':
			terminal_directory_remove(term, parameters);
			break;
		case 'search':
			terminal_file_search(term, parameters);
			break;
		case 'version':
			term.writeln('Orb v' + $('div.desktop').attr('version'));
			terminal_done(term);
			break;
		case 'wallpaper':
			terminal_setting_wallpaper(term, parameters);
			break;
		default:
			/* Find available applications
			 */
			if (terminal_applications == null) {
				terminal_applications = [];
				$('script').each(function() {
					var src = $(this).attr('src').split('/');
					if (src[1] == 'apps') {
						terminal_applications.push(src[2]);
					}
				});
			}

			if (terminal_applications.includes(command)) {
				/* Execute application
				 */
				command += '_open';
				var file = parameters[0];
				if (file != undefined) {
					if (file.substr(0, 1) != '/') {
						file = terminal_combine_directories(term.path, file);
					}
				}
				window[command](file);
			} else {
				term.writeln('Unknown command.');
			}

			terminal_done(term);
	}
}
