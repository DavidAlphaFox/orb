/* Orb browser application
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications,
 *
 * Always use browser_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

function browser_get_url(browser_window) {
	var url = browser_window.find('iframe')[0].contentWindow.location.href;
	var parts = url.split('=');

	if (parts.length > 1) {
		parts.shift();

		return parts.join('=');
	} else {
		parts = url.split('/');
		for (var i = 0; i < 6; i++) parts.shift();

		return 'file://' + parts.join('/');
	}
}

function browser_open_url(browser_window, url) {
	if (url.substr(0, 7) == 'file://') {
		url = orb_download_url(url.substr(7));
	} else {
		if ((url.substr(0, 5) != 'http:') &&(url.substr(0, 6) != 'https:')) {
			url = 'https://' + url;
		}

		url = '/apps/browser/browser.php?url=' + url;
	}

	browser_window.find('iframe').attr('src', url);
}

function browser_menu_click(browser_window, item) {
	browser_window = browser_window.find('div.browser');

	switch (item) {
		case 'Save URL':
			orb_file_dialog('Save URL', function(filename) {
				if (orb_file_extension(filename) != 'url') {
					filename += '.url';
				}
				var content = '[InternetShortcut]\r\nURL=' + browser_window.find('div.url input').val();
				orb_file_save(filename, content);
			}, 'Bookmarks');
			break;
		case 'Set as homepage':
			if (confirm('Set this page as the homepage?') == false) {
				break;
			}

			var url = browser_get_url(browser_window);
			orb_setting_set('applications/browser/homepage', url);
			break;
		case 'Exit':
			browser_window.close();
			break;
		case 'About':
			orb_alert('Web browser via Orb backend proxy\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function browser_open_icon(icon) {
	var filename = orb_icon_to_filename(icon);

	if (filename == undefined) {
		return;
	}

	browser_open(filename);
}

function browser_open(filename = undefined) {
	var window_content =
		'<div class="browser">' +
		'<div class="topbar"><button class="home"><span class="fa fa-home"></span></button><button disabled="disabled" class="previous history">&lt;</button><button disabled="disabled" class="next history">&gt;</button>' + 
		'<div class="url input-group"><input type="text" placeholder="Enter URL" class="form-control" /><span class="input-group-btn"><button class="btn btn-default">Go</button></span></div></div>' +
		'<div class="website"><iframe></iframe></div>' +
		'</div>';

	var browser_window = $(window_content).orb_window({
		header:'Browser',
		icon: '/apps/browser/browser.png',
		width: 800,
		height: 500,
		menu: {
			'File': [ 'Save URL', 'Set as homepage', '-', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: browser_menu_click
	});

	/* Home button
	 */
	orb_setting_get('applications/browser/homepage', function(value) {
		browser_window.find('button.home').click(function() {
			browser_open_url(browser_window, value);
		});
	});

	/* Browser history buttons
	 */
	browser_window.data('history', []);
	browser_window.data('history_pointer', -1);
	browser_window.data('history_add', false);

	browser_window.find('button.previous').click(function() {
		var pointer = browser_window.data('history_pointer');

		if (pointer == 0) {
			return;
		}

		var history = browser_window.data('history');

		browser_window.data('history_add', false);
		browser_open_url(browser_window, history[--pointer]);
		browser_window.data('history_pointer', pointer);

		if (pointer == 0) {
			browser_window.find('button.previous').attr('disabled', true);
		}

		browser_window.find('button.next').attr('disabled', false);
	});

	browser_window.find('button.next').click(function() {
		var history = browser_window.data('history');
		var pointer = browser_window.data('history_pointer');
		var max_ptr = history.length - 1;

		if (pointer == max_ptr) {
			return;
		}

		browser_window.data('history_add', false);
		browser_open_url(browser_window, history[++pointer]);
		browser_window.data('history_pointer', pointer);

		if (pointer == max_ptr) {
			browser_window.find('button.next').attr('disabled', true);
		}

		browser_window.find('button.previous').attr('disabled', false);
	});

	/* Browser URL bar
	 */
	browser_window.find('div.url button').click(function() {
		var url = browser_window.find('div.url input').val();

		browser_open_url(browser_window, url);
	});

	browser_window.find('div.url input').focus(function(event) {
		$(this).select();
	});

	browser_window.find('div.url input').keypress(function(event) {
		if (event.which == 13) {
			browser_window.find('div.url button').trigger('click');
		}
	});

	/* Browser content 
	 */
	browser_window.find('iframe').on('load', function() {
		var url = browser_get_url(browser_window);
		var url_bar = browser_window.find('div.url input');

		url_bar.val(url);

		/* History
		 */
		var add = browser_window.data('history_add');
		browser_window.data('history_add', true);

		if (add == false) {
			return;
		}

		var history = browser_window.data('history');
		var pointer = browser_window.data('history_pointer');

		while (history.length - 1 > pointer) {
			history.pop();
		}

		history.push(url);
		pointer = history.length - 1;

		browser_window.data('history', history);
		browser_window.data('history_pointer', pointer);

		browser_window.find('button.next').attr('disabled', true);

		if (pointer > 0) {
			browser_window.find('button.previous').attr('disabled', false);
		}
	});

	browser_window.open();

	if (filename != undefined) {
		if (orb_file_extension(filename) == 'html') {
			browser_open_url(browser_window, 'file://' + filename);
		} else {
			orb_file_open(filename, function(content) {
				var lines = content.split('\n');
				lines.forEach(function(line) {
					var key = line.substr(0, 4).toLowerCase();
					if (key == 'url=') {
						browser_open_url(browser_window, line.substr(4));
					}
				});
			});
		}
	} else {
		orb_setting_get('applications/browser/homepage', function(value) {
			browser_open_url(browser_window, value);
		});
	}
}

$(document).ready(function() {
	var icon = '/apps/browser/browser.png';
	orb_startmenu_add('Browser', icon, browser_open);
	orb_upon_file_open('url', browser_open, icon);

	orb_contextmenu_extra_item('html', 'View with Browser', 'eye', browser_open_icon);
});
