<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	class user_website {
		private $view = null;
		private $username = null;

		/* Constructor
		 *
		 * INPUT:  object view
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($view) {
			$this->view = $view;
		}

		/* Check if user website is requested or not
		 *
		 * INPUT:  -
		 * OUTPUT: bool user website requested
		 * ERROR:  -
		 */
		public function requested() {
			if (is_false(USER_WEBSITES)) {
				return false;
			} else if (AUTHENTICATION == "none") {
				return false;
			}

			if ($_SERVER["REQUEST_URI"] == "/") {
				return false;
			}

			$parts = explode("/", $_SERVER["REQUEST_URI"]);
			$username = $parts[1];

			if (substr($username, 0, 1) != "~") {
				return false;
			}

			$username = substr($username, 1);

			$users = file(PASSWORD_FILE);
			foreach ($users as $user) {
				$user = explode(":", $user);

				if ($user[0] == $username) {
					$this->username = $username;
					break;
				}
			}

			return $this->username != null;
		}

		/* Get mime type of requested file
		 *
		 * INPUT:  string requested file
		 * OUTPUT: string mime type
		 * ERROR:  -
		 */
		private function get_mimetype($path) {
			$default = "application/octet-stream";

			$pathinfo = pathinfo($path);
			if (isset($pathinfo["extension"]) == false) {
				return $default;
			}

			if (($fp = fopen("/etc/mime.types", "r")) == false) {
				return $default;
			}

			while (($line = fgets($fp)) !== false) {
				$line = preg_replace('/#.*/', "", $line);
				$line = trim($line);
				if ($line == "") {
					continue;
				}

				$line = preg_replace('/\s+/', ' ', $line);
				$extensions = explode(" ", $line);
				$mimetype = array_shift($extensions);

				if (in_array($pathinfo["extension"], $extensions)) {
					fclose($fp);

					return $mimetype;
				}
			}

			fclose($fp);

			return $default;
		}

		/* Send 404 error to browser
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function show_error() {
			$this->view->add_css("bootstrap.css");
			$this->view->add_css("bootstrap-theme.css");
			$this->view->add_css("notification.css");
			$this->view->return_error(404);

			return "error";
		}

		/* Send requested user website to browser
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  string view file
		 */
		public function show() {
			$file = substr($_SERVER["REQUEST_URI"], strlen($this->username) + 2);

			if ((strpos($file, "/.") !== false) || (strpos($file, "../") !== false)) {
				return $this->show_error();
			}

			$path = __DIR__."/../".HOME_DIRECTORIES."/".$this->username."/Website".$file;

			if (is_dir($path)) {
				if (substr($path, -1) == "/") {
					$path .= "index.html";
				} else {
					$this->view->return_error(301);
					header("Location: ".$_SERVER["REQUEST_URI"]."/");
					exit;
				}
			}

			if (file_exists($path) == false) {
				return $this->show_error();
			}

			ob_end_clean();

			$mimetype = $this->get_mimetype($path);

			header("Content-Type: ".$mimetype);
			readfile($path);

			exit;
		}
	}
?>
