<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	if (defined("ORB_VERSION") == false) exit;

	class terminal extends orb_backend {
		/* DNS lookup
		 */
		public function get_dns() {
			$host = $this->parameters[0];

			if (valid_input($host, VALIDATE_NUMBERS.".:", VALIDATE_NONEMPTY)) {
				if (($host = gethostbyaddr($host)) == false) {
					$this->view->return_error(404);
				} else {
					$this->view->add_tag("result", $host);
				}
			} else if (valid_input($host, VALIDATE_NONCAPITALS.VALIDATE_NUMBERS.".-_", VALIDATE_NONEMPTY)) {
				if (($record = dns_get_record($host, DNS_ALL)) == false) {
					$this->view->return_error(404);
				} else foreach ($record as $item) {
					$this->view->open_tag("result");
					foreach ($item as $key => $value) {
						if (is_array($value)) {
							continue;
						}
						$this->view->add_tag($key, $value);
					}
					$this->view->close_tag();
				}
			} else {
				$this->view->return_error(406);
			}
		}

		/* Ping
		 */
		public function get_ping() {
			$host = $this->parameters[0];

			if (valid_input($host, VALIDATE_NONCAPITALS.VALIDATE_NUMBERS.".-_", VALIDATE_NONEMPTY) == false) {
				$this->view->return_error(406);
				return;
			}

			$lines = array();
			exec("ping -w".TERMINAL_NETWORK_TIMEOUT." ".$host, $lines);

			foreach ($lines as $line) {
				$this->view->add_tag("line", $line);
			}
		}

		/* Port reachability
		 */
		public function get_port() {
			$host = $this->parameters[0];
			$port = $this->parameters[1];

			if (dns_get_record($host) == false) {
				$this->view->return_error(404);
				return;
			}

			ob_start();
			$socket = fsockopen($host, $port, $errnum, $errstr, TERMINAL_NETWORK_TIMEOUT);
			ob_end_clean();

			if ($socket == false) {
				$this->view->return_error(403);
			} else {
				fclose($socket);
			}
		}

		private function hash_password($password) {
			$hash = "\$5\$".random_string(20);

			return crypt($password, $hash);
		}

		/* Set Hiawatha password
		 */
		public function post_password() {
			$directory = "/".trim($_POST["path"] ?? "", "/ ");

			if ($this->valid_filename($directory) == false) {
				$this->view->return_error(400);
				return;
			}

			$directory = $this->home_directory.$directory."/";

			if (isset($_POST["username"]) == false) {
				if (file_exists($directory.".password") == false) {
					$this->view->return_error(400);
					return;
				}

				$file = file($directory.".password");
				foreach ($file as $line) {
					list($username) = explode(":", $line, 2);
					$this->view->add_tag("username", $username);
				}

				return;
			}

			if ($_POST["username"] == "remove") {
				unlink($directory.".hiawatha");
				unlink($directory.".password");
				return;
			}

			/* File .hiawatha
			 */
			if (file_exists($directory.".hiawatha") == false) {
				if (($fp = fopen($directory.".hiawatha", "w")) == false) {
					$this->view->return_error(403);
					return;
				}
				
				fputs($fp, "PasswordFile = basic:".$directory.".password\n");

				fclose($fp);
			}

			/* File .password
			 */
			$found = false;
			if (file_exists($directory.".password")) {
				$passwords = file_get_contents($directory.".password");
				$passwords = explode("\n", trim($passwords));

				foreach ($passwords as $i => $line) {
					list($username, $password) = explode(":", $line, 2);
					if ($username == $_POST["username"]) {
						if ($_POST["password"] == "") {
							unset($passwords[$i]);
						} else {
							$passwords[$i] = $username.":".$this->hash_password($_POST["password"]);
						}
						$found = true;
						break;
					}
				}
			} else {
				$passwords = array();
			}

			if (($found == false) && ($_POST["password"] != "")) {
				array_push($passwords, $_POST["username"].":".$this->hash_password($_POST["password"]));
			}

			if (($fp = fopen($directory.".password", "w")) == false) {
				$this->view->return_error(403);
				return;
			}

			fputs($fp, implode("\n", $passwords)."\n");

			fclose($fp);
		}

		/* Search file
		 */
		public function post_search($directory = ".") {
			if (($dp = opendir($this->home_directory."/".$directory)) == false) {
				return;
			}

			$search = strtolower($_POST["search"]);

			while (($file = readdir($dp)) != false) {
				if (substr($file, 0, 1) == ".") {
					continue;
				}

				$path = $this->home_directory."/".$directory."/".$file;

				$is_dir = is_dir($path);

				if ($is_dir) {
					$this->post_search($directory."/".$file);
				}

				if (strpos(strtolower($file), $search) !== false) {
					$attr = array("type" => $is_dir ? "file" : "dir");
					$this->view->add_tag("file", substr($directory."/".$file, 2), $attr);
				}

			}

			closedir($dp);
		}

		/* File source
		 */
		public function get_source() {
			$files = array(
				"public/js/desktop.js",
				"public/js/orb.js",
				"public/js/windows.js");

			$file = $files[random_int(0, count($files) - 1)];

			$this->view->add_tag("source", file_get_contents("../".$file));
		}
	}
?>
