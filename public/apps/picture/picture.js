/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var picture_valid_extensions = [ 'gif', 'jpeg', 'jpg', 'png', 'webp' ];

function picture_show(picture_window, filename) {
	picture_window.css('background-image', 'url(' + orb_download_url(filename) + ')');

	picture_window.find('div.name').text(orb_file_filename(filename));

	var directory = orb_file_dirname(filename);

	orb_directory_list(directory, function(items) {
		var head = [];
		var tail = [];
		var found = false;

		items.forEach(function(item) {
			var extension = orb_file_extension(item.name);

			if (extension != false) {
				extension = extension.toLowerCase();
			}

			if (picture_valid_extensions.includes(extension)) {
				var picture = directory + '/' + item.name;

				if (found) {
					head.push(picture);
				} else if (picture == filename) {
					head.push(picture);
					found = true;
				} else {
					tail.push(picture);
				}
			}
		});

		var list = head.concat(tail);

		picture_window.data('list', list);
	}, function(result) {
		picture_window.data('list', []);
	});
}

function picture_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return picture_valid_extensions.includes(extension.toLowerCase());
}

function picture_open(filename = undefined) {
	var window_content = '<div class="picture">' +
		'<button class="btn left"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></button>' +
		'<button class="btn right"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>' +
		'<div class="name"></div>' +
		'</div>';

	var picture_window = $(window_content).orb_window({
		header:'Picture',
		icon: '/apps/picture/picture.png',
		width: 800,
		height: 500,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: picture_menu_click
	});

	picture_window.data('list', []);

	picture_window.find('button.left').click(function() {
		var list = picture_window.data('list');
		if (list.length == 0) {
			return;
		}

		var picture = list.shift();
		list.unshift(list.pop());
		picture_window.data('list', list);

		picture_show(picture_window, list[0]);
	});

	picture_window.find('button.right').click(function() {
		var list = picture_window.data('list');
		if (list.length == 0) {
			return;
		}

		var picture = list.shift();
		list.push(list.shift);
		picture_window.data('list', list);

		picture_show(picture_window, list[0]);
	});

	picture_window.open();

	if (filename != undefined) {
		if (picture_valid_extension(filename) == false) {
			orb_alert('Invalid file type.');
		} else {
			picture_show(picture_window, filename);
		}
	}
}

function picture_menu_click(picture_window, item) {
	picture_window = picture_window.find('div.picture');

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (picture_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					picture_show(picture_window, filename);
				}
			}, 'Pictures');
			break;
		case 'Exit':
			picture_window.close();
			break;
		case 'About':
			orb_alert('Picture viewer\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

$(document).ready(function() {
	var icon = '/apps/picture/picture.png';
	orb_startmenu_add('Picture', icon, picture_open);

	picture_valid_extensions.forEach(function(extension) {
		orb_upon_file_open(extension, picture_open, icon);
	});
});
