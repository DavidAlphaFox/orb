/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_file_cat(term, files) {
	if (files.length == 0) {
		term.writeln('Arguments missing.');
		terminal_done(term);
		return;
	}

	var count = files.length;

	var cat_extensions = [ 'txt', 'ots' ];

	files.forEach(function(file) {
		var extension = orb_file_extension(file);

		if (cat_extensions.includes(extension) == false) {
			term.writeln('Incorrect file type.');
			if (--count == 0) {	
				terminal_done(term);
			}
			return;
		}

		file = terminal_combine_directories(term.path, file);

		orb_file_open(file, function(content) {
			var lines = content.split('\n');
			lines.forEach(function(line) {
				term.writeln(line);
			});

			if (--count == 0) {	
				terminal_done(term);
			}
		}, function() {
			term.writeln('File not found.');

			if (--count == 0) {	
				terminal_done(term);
			}
		});
	});
}

function terminal_file_open(term, files) {
	if (files.length == 0) {
		files = [ '' ];
	}

	files.forEach(function(file) {
		file = terminal_combine_directories(term.path, file);

		orb_file_type(file, function(type) {
			if (type == 'file') {
				var extension = orb_file_extension(file);
				var handler = orb_get_file_handler(extension);

				if (handler != undefined) {
					handler(file);
				} else {
					window.open(orb_download_url(file));
				}

				terminal_done(term);
			} else {
				var handler = orb_get_directory_handler();
				handler(file);

				terminal_done(term);
			}
		}, function() {
			term.writeln('File not found.');
			terminal_done(term);
		});
	});
}

function terminal_file_copy(term, files) {
	if (terminal_argument_count(term, files, 2) == false) {	
		return false;
	}

	var source = files.shift();
	var destination = files.shift();

	if (source.substr(0, 1) != '/') {
		source = terminal_combine_directories(term.path, source);
	}

	if (destination == '.') {
		destination = term.path;
	} else if (destination.substr(0, 1) != '/') {
		destination = terminal_combine_directories(term.path, destination);
	}

	orb_file_copy(source, destination, function() {
		terminal_done(term);
	}, function() {
		term.writeln('Error copying file.');
		terminal_done(term);
	});
}

function terminal_file_edit(term, files) {
	if (terminal_argument_count(term, files, 1) == false) {
		return false;
	}

	var file = term.path + '/' + files[0];
	var editor = $('div.desktop').attr('editor');

	window[editor](file);

	terminal_done(term);
}

function terminal_file_move(term, files) {
	if (terminal_argument_count(term, files, 2) == false) {
		return false;
	}

	var source = files.shift();
	var destination = files.shift();

	if (source.substr(0, 1) != '/') {
		source = terminal_combine_directories(term.path, source);
	}

	if (destination == '.') {
		destination = term.path;
	} else if (destination.substr(0, 1) != '/') {
		destination = terminal_combine_directories(term.path, destination);
	}

	orb_file_move(source, destination, function() {
		terminal_done(term);
	}, function() {
		term.writeln('Error moving file.');
		terminal_done(term);
	});
}

function terminal_file_remove(term, files) {
	if (files.length == 0) {
		term.writeln('Arguments missing.');
		terminal_done(term);
		return;
	}

	var count = files.length;

	files.forEach(function(file) {
		file = terminal_combine_directories(term.path, file);

		orb_file_remove(file, function() {
			if (--count == 0) {	
				terminal_done(term);
			}
		}, function() {
			term.writeln('Error removing file.');

			if (--count == 0) {	
				terminal_done(term);
			}
		});
	});
}

function terminal_file_search(term, files) {
	if (terminal_argument_count(term, files, 1) == false) {
		return false;
	}

	$.post('/terminal/search', {
		search: files[0]
	}).done(function(data) {
		$(data).find('file').each(function() {
			term.writeln($(this).text());
		});

		terminal_done(term);
	}).fail(function() {
		terminal_done(term);
	});
}
