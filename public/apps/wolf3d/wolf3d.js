function wolf3d_open(filename = undefined) {
	var window_content =
		'<div class="wolf3d"><iframe id="wolf3d" src="/apps/wolf3d/wolf3d.html"></iframe></div>';

	var wolf3d_window = $(window_content).orb_window({
		header:'Wolfenstein 3D',
		icon: '/apps/wolf3d/wolf3d.png',
		width: 660,
		height: 400,
		maximize: false,
		resize: false
	});

	wolf3d_window.open();
}

$(document).ready(function() {
	orb_startmenu_add('Wolfenstein 3D', '/apps/wolf3d/wolf3d.png', wolf3d_open);
});
